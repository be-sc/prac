# Prolog for comps_with_deps
if (CMAKE_VERSION VERSION_LESS 3.20)
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "comps_with_deps requires CMake 3.20 or later. "
        "You are running version ${CMAKE_VERSION}."
    )
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()

set(requested_components ${${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS})

set(missing_comps)
foreach (comp IN LISTS requested_components)
    if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_${comp})
        if (NOT EXISTS "${CMAKE_CURRENT_LIST_DIR}/comps_with_deps-component-${comp}.cmake")
            set(missing_comps "${missing_comps} ${comp}")
        endif()
    endif()
endforeach()
if (missing_comps)
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "comps_with_deps is missing required components:${missing_comps}")
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()
unset(missing_comps)

# In CMake versions older than 3.15 `find_dependency()` does nothing if the `<dep>_FOUND`
# variable is true. That can lead to components of a dependency not being checked when
# different `find_dependency()` calls request different components of the same package.
# For example: When first requesting Boost filesystem and then requesting Boost asio, asio will
# not be checked and not be imported.
# Bugtracker issue: https://gitlab.kitware.com/cmake/cmake/issues/17583
if (CMAKE_VERSION VERSION_LESS 3.15)
    # older CMake version : reimpl of find_dependency() without the flawed optimization
    macro(prac_find_dependency dep_name)
        set(pracprivate_managed_args)
        if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
            list(APPEND pracprivate_managed_args REQUIRED)
        endif()
        if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
            list(APPEND pracprivate_managed_args QUIET)
        endif()

        find_package(${dep_name} ${ARGN} ${pracprivate_managed_args})
        unset(pracprivate_managed_args)

        if (NOT ${dep_name}_FOUND)
            set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
                "Finding comps_with_deps failed because its dependency ${dep_name} could not be found.")
            set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
            return()
        endif()
    endmacro()
else()
    # recent enough CMake version where find_dependency() can be used directly
    include(CMakeFindDependencyMacro)
    macro(prac_find_dependency)
        find_dependency(${ARGV})
    endmacro()
endif()
# global dependencies
prac_find_dependency(Qt5 5.15 REQUIRED COMPONENTS Core Gui)
# component `subcomponent` dependencies
if ("subcomponent" IN_LIST requested_components)
    prac_find_dependency(Catch2 REQUIRED)
endif()

include("${CMAKE_CURRENT_LIST_DIR}/comps_with_deps-targets.cmake")
foreach (comp IN LISTS requested_components)
    include("${CMAKE_CURRENT_LIST_DIR}/comps_with_deps-component-${comp}.cmake" OPTIONAL)
endforeach()

# Epilog for comps_with_deps
