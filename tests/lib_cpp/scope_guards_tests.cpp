// Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#include <catch2/catch.hpp>

#define PRAC_UNPREFIXED_MACROS
#include <prac/scope_guards.hpp>

namespace
{
static constexpr char tag[] = "[scope_guards]";
}


TEST_CASE("SCOPE_EXIT always fires", tag)
{
    int guard_fired_count = 0;

    {  // scope will be executed without exception
        SCOPE_EXIT
        {
            ++guard_fired_count;
        };
    }
    REQUIRE(guard_fired_count == 1);

    try {
        {  // scope will be executed with exception
            PRAC_SCOPE_EXIT
            {
                ++guard_fired_count;
            };
            throw 1;
        }
    }
    catch (int) {
        // need to neutralize the exception before the end of the test case
    }
    REQUIRE(guard_fired_count == 2);
}

TEST_CASE("SCOPE_SUCCESS only fires on exit w/o exception", tag)
{
    int guard_fired_count = 0;

    {
        SCOPE_SUCCESS
        {
            ++guard_fired_count;
        };
    }
    REQUIRE(guard_fired_count == 1);

    try {
        {
            PRAC_SCOPE_SUCCESS
            {
                ++guard_fired_count;
            };
            throw 1;
        }
    }
    catch (int) {
    }
    REQUIRE(guard_fired_count == 1);
}

TEST_CASE("SCOPE_FAIL only fires on exit with exception", tag)
{
    int guard_fired_count = 0;

    {
        SCOPE_FAIL
        {
            ++guard_fired_count;
        };
    }
    REQUIRE(guard_fired_count == 0);

    try {
        {
            PRAC_SCOPE_FAIL
            {
                ++guard_fired_count;
            };
            throw 1;
        }
    }
    catch (int) {
    }
    REQUIRE(guard_fired_count == 1);
}
