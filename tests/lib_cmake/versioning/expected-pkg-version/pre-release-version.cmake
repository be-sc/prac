# A tweak version part may be requested through find_package(). Semantic versioning does not have
# an equivalent version part. Tweak is completely ignored for the version check.

macro(define_versions)  # helper for version ranges
    if (PACKAGE_FIND_VERSION_MIN_COUNT GREATER 2)
        set(min_ver "${PACKAGE_FIND_VERSION_MIN_MAJOR}.${PACKAGE_FIND_VERSION_MIN_MINOR}.${PACKAGE_FIND_VERSION_MIN_PATCH}")
        set(min_ver_built "4.5.6")
    elseif (PACKAGE_FIND_VERSION_MIN_COUNT EQUAL 2)
        set(min_ver "${PACKAGE_FIND_VERSION_MIN_MAJOR}.${PACKAGE_FIND_VERSION_MIN_MINOR}")
        set(min_ver_built "4.5")
    else()
        set(min_ver "${PACKAGE_FIND_VERSION_MIN_MAJOR}")
        set(min_ver_built "4")
    endif()

    if (PACKAGE_FIND_VERSION_MAX_COUNT GREATER 2)
        set(max_ver "${PACKAGE_FIND_VERSION_MAX_MAJOR}.${PACKAGE_FIND_VERSION_MAX_MINOR}.${PACKAGE_FIND_VERSION_MAX_PATCH}")
        set(max_ver_built "4.5.6")
    elseif(PACKAGE_FIND_VERSION_MAX_COUNT EQUAL 2)
        set(max_ver "${PACKAGE_FIND_VERSION_MAX_MAJOR}.${PACKAGE_FIND_VERSION_MAX_MINOR}")
        set(max_ver_built "4.5")
    else()
        set(max_ver "${PACKAGE_FIND_VERSION_MAX_MAJOR}")
        set(max_ver_built "4")
    endif()
endmacro()


set(PACKAGE_VERSION "4.5.6-pre+meta")
set(PACKAGE_VERSION_COMPATIBLE FALSE)
set(PACKAGE_VERSION_EXACT FALSE)


if (PACKAGE_FIND_VERSION_RANGE)
    # find_package() called with a version range (CMake 3.19+)
    define_versions()
    set(min_ver_satisfied FALSE)

    if ("${PACKAGE_FIND_VERSION_RANGE_MIN}" STREQUAL "INCLUDE")
        if ("${min_ver}" VERSION_LESS_EQUAL "${min_ver_built}")
            set(min_ver_satisfied TRUE)
        endif()
    elseif ("${PACKAGE_FIND_VERSION_RANGE_MIN}" STREQUAL "EXCLUDE")
        if ("${min_ver}" VERSION_LESS "${min_ver_built}")
            set(min_ver_satisfied TRUE)
        endif()
    endif()

    if (min_ver_satisfied)
        if ("${PACKAGE_FIND_VERSION_RANGE_MAX}" STREQUAL "INCLUDE")
            if ("${max_ver}" VERSION_GREATER_EQUAL "${max_ver_built}")
                set(PACKAGE_VERSION_COMPATIBLE TRUE)
            endif()
        elseif("${PACKAGE_FIND_VERSION_RANGE_MAX}" STREQUAL "EXCLUDE")
            if ("${max_ver}" VERSION_GREATER "${max_ver_built}")
                set(PACKAGE_VERSION_COMPATIBLE TRUE)
            endif()
        endif()
    endif()

elseif (PACKAGE_FIND_VERSION_COUNT EQUAL 0)
    # find_package() called without a version
    set(PACKAGE_VERSION_COMPATIBLE TRUE)

elseif (PACKAGE_FIND_VERSION_MAJOR EQUAL 4)
    # find_package() called with a single version
    if (PACKAGE_FIND_VERSION_MINOR EQUAL 5)
        set(PACKAGE_VERSION_COMPATIBLE TRUE)
        
        # About the exact version check:
        # This is a pre-release version. find_package() does not allow requesting a pre-release
        # version. Thus we cannot reliably check for an exact version match. We are conservative
        # and never claim an exact match.
    elseif (PACKAGE_FIND_VERSION_MINOR LESS 5)
        set(PACKAGE_VERSION_COMPATIBLE TRUE)
    endif()
endif()


