# Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#[===[.rst:
Provides functionality for automating target installation and package export. CMake requires quite a bit of work to get a full-featured installation working correctly.

For the exported config package this module uses the common practice of creating a top-level *<package>-config.cmake* file for custom code, which uses ``include()`` to pull in the main target config file created by ``install(EXPORT)``. That file is renamed to *<package>-targets.cmake*.

An often needed piece of custom code is automating finding transitive dependencies by placing the appropriate ``find_dependency()`` calls in the exported config package. The ``prac_find_package_and_export()`` function in this module provides a way to record ``find_package()`` calls relevant for installation.

About CMake’s two kinds of COMPONENT
------------------------------------

CMake has ``install(COMPONENT)`` and ``find_package(COMPONENTS)``. Those are not the same thing! If you have ``install(COMPONENT foo)`` in your CMake code that does not mean someone else can call ``find_package(COMPONENTS foo)`` later. Those are two different and completely separate kinds of component.

**Install components** give you control over which parts of a project to install. Different components usually have different purposes. Think of Linux distributions like Ubuntu that split up projects into a productive component – the one you install to use the software – and a development component – the one you install additionally if you want to use that software in programming.

**Package components** give you control over which parts of an already installed software you want to depend on in your own project. They are commonly used to structure large projects. Qt and Boost are well known examples. In contrast to install components CMake has no built-in support for package components other than the ``COMPONENTS`` parameter of ``find_package()``. The config package must handle such a component list by itself. CMake does not help. The extensions in this file do.

Look for the ``PKG_COMPONENT`` parameter in several functions to define the content of a set of package components. ``prac_install_config_package()`` generates a config package with code for handling those components during a ``find_package()`` call.
]===]
include_guard(GLOBAL)

set(pracprivateInstallation_here_dir "${CMAKE_CURRENT_LIST_DIR}")
include("${pracprivateInstallation_here_dir}/Versioning.cmake")


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Calls ``find_package()`` with the given arguments and records the call for later use in an config package. ::

    prac_find_package_and_export(
        [EXPORT <name>]
        [PKG_COMPONENT <comp>]
        <find_package-args...>
    )

Parameters:

``EXPORT <name>``
    Optional name of the export set the ``find_package()`` call belongs to. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``PKG_COMPONENT <comp>``
    Optional name of a package component below the given export set. Defaults to the nameless top-level component.

Outputs:

Calls are logged to a global property and are automatically used by ``prac_install_config_package()`` when installing the export set as a config package.
]]
macro(prac_find_package_and_export)
    pracprivate_register_find_dependency_call(${ARGV})
    find_package(${pracprivateFpae_fpargs})
    unset(pracprivateFpae_fpargs)
endmacro()
function(pracprivate_register_find_dependency_call)
    cmake_parse_arguments(PARSE_ARGV 0 "" "" "EXPORT;PKG_COMPONENT" "")
    set(pracprivateFpae_fpargs ${_UNPARSED_ARGUMENTS} PARENT_SCOPE)

    pracprivate_define_export_name()

    list(JOIN _UNPARSED_ARGUMENTS " " args_for_find_dependency)
    string(JOIN "_" propname "pracprivate_logged_dependency_calls" ${export_name} ${_PKG_COMPONENT})

    set_property(
        GLOBAL APPEND PROPERTY ${propname} "prac_find_dependency(${args_for_find_dependency})\n"
    )
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Defines installation destination directories. Includes ``GNUInstallDirs`` to define the usual ones. Also defines additional ones. ::

    prac_define_install_dirs()

Additional install destinations:

``PRAC_INSTALL_DOCROOTDIR``
    Similar to ``CMAKE_INSTALL_DOCDIR`` but does not include the ``PROJECT_NAME`` as its last subdirectory. Default: *CMAKE_INSTALL_DATAROOTDIR/doc*.
``PRAC_INSTALL_EXPORTROOTDIR``
    Defines the project independent root directory for export sets and package config files. Default: *CMAKE_INSTALL_LIBDIR/cmake*.
]]
macro(prac_define_install_dirs)
    include(GNUInstallDirs)

    get_filename_component(pracprivateDid_docroot "${CMAKE_INSTALL_DOCDIR}" DIRECTORY)
    set(
        PRAC_INSTALL_DOCROOTDIR "${pracprivateDid_docroot}"
        CACHE PATH "project independent documentation root (DATAROOTDIR/doc)"
    )
    unset(pracprivateDid_docroot)

    prac_define_export_root_dir()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Defines only the ``PRAC_INSTALL_EXPORTROOTDIR`` installation directory. See ``prac_define_install_dirs()`` for details about its location. ::

    prac_define_export_root_dir()
]]
macro(prac_define_export_root_dir)
    pracprivate_define_export_root_dir(pracprivateDerd_exroot)
    set(
        PRAC_INSTALL_EXPORTROOTDIR "${pracprivateDerd_exroot}"
        CACHE PATH "export sets root (LIBDIR/cmake)"
    )
    unset(pracprivateDerd_exroot)
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
An extension to the built-in ``install(TARGETS)``. Installs and associates targets with an export set and optionally one of its package components. ::

    prac_install_payload(
        TARGETS <target>...
        [EXPORT <name>]
        [PKG_COMPONENT <comp>]
        [<install-TARGETS-args>...]
    )

Parameters:

``TARGETS <targets>...``
    A list of one or more existing targets.
``EXPORT <name>``
    Optional name of the export set the target will belong to. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``PKG_COMPONENT <comp>``
    Optional name of a package component below the given export set. Defaults to the nameless top-level component.
all other arguments
    passed verbatim ``install(TARGETS)``
]]
function(prac_install_payload)
    set(switches)
    set(one_value_args EXPORT PKG_COMPONENT)
    set(multi_value_args TARGETS)
    cmake_parse_arguments(PARSE_ARGV 0 "" "${switches}" "${one_value_args}" "${multi_value_args}")

    pracprivate_define_export_name()

    # The properties (toplevel flag and component name list) are needed later by install_export_set()
    # to figure out which install(EXPORT) commands to run.
    if ("${_PKG_COMPONENT}" STREQUAL "")  # installing toplevel targets
        set(fullex_name "${export_name}")
        set_property(GLOBAL APPEND PROPERTY pracprivate_install_pkgtoplevel_${export_name} TRUE)
    else()
        set(fullex_name "${export_name}_${_PKG_COMPONENT}")
        set_property(GLOBAL APPEND PROPERTY pracprivate_install_pkgcomps_${export_name} "${_PKG_COMPONENT}")
    endif()

    install(TARGETS ${_TARGETS} EXPORT "${fullex_name}" ${_UNPARSED_ARGUMENTS})
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Customizes the ``install(EXPORT)`` command for the given export set and optionally one of its package components. ::

    prac_configure_config_package(
        [EXPORT <name>]
        [PKG_COMPONENT <comp>]
        [<install-EXPORT-args>...]
    )

Must be called before the ``prac_install_config_package()`` for the same export set.

Parameters:

``EXPORT <name>``
    Optional name of the export set. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``PKG_COMPONENT <comp>``
    Optional name of a package component below the given export set. Defaults to the nameless top-level component.
all other arguments
    Recorded verbatim for later passing to ``install(EXPORT)``.
]]
function(prac_configure_config_package)
    set(switches)
    set(one_value_args EXPORT PKG_COMPONENT DESTINATION NAMESPACE FILE)
    set(multi_value_args)
    cmake_parse_arguments(PARSE_ARGV 0 "" "${switches}" "${one_value_args}" "${multi_value_args}")

    if (NOT "${_DESTINATION}${_NAMESPACE}${_FILE}" STREQUAL "")
        message(
            FATAL_ERROR
            "The install(EXPORT) arguments DESTINATION, NAMESPACE and FILE cannot be specified manually."
        )
    endif()

    pracprivate_define_export_name()

    if ("${_PKG_COMPONENT}" STREQUAL "")  # configuration for toplevel targets
        set(prop_name "pracprivate_install_instexpargs_${export_name}")
    else()
        set(prop_name "pracprivate_install_instexpargs_${export_name}_${_PKG_COMPONENT}")
    endif()

    set_property(GLOBAL APPEND PROPERTY ${prop_name} ${_UNPARSED_ARGUMENTS})
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Configures the content of a set of config package files and creates those files. ::

    prac_install_config_package(
        [EXPORT <name>]
        [NAMESPACE <ns> | NO_NAMESPACE]

        [TEMPLATE_FILE <filepath>]
        [RESOLVE_PKG_COMPONENTS_TMPLVAR <varname>]
        [PROLOG_TMPLVAR <varname]
        [EPILOG_TMPLVAR <varname>]
        [IMPORTER_MIN_CMAKE_VERSION <version>]

        [ # version configuration
          NO_VERSION | [
            [VERSION {<major.minor.patch>|<semantic_version>}]
            [COMPATIBILITY {<built-in_specifier>|SemanticVersion}]
            [ARCH_INDEPENDENT]
          ]
        ]
    )

Parameters for configuring the exported config package:

``EXPORT <name>``
    Optional name of the export set. Becomes the name of the exported config package. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``NAMESPACE <ns>`` and ``NO_NAMESPACE``
    Custom namespace control. By default the exported targets are put into a namespace called the same as the export set. Disable that behaviour with ``NO_NAMESPACE`` or pick a custom one with ``NAMESPACE``. The custom name can be given with or without a trailing double colon (``::``). If needed Prac adds it automatically.
``TEMPLATE_FILE <filepath>``
    Path to the template file used to generate the package config file. Only needed under special circumstances. In most situations the default template in combination with the ``_TMPLVAR`` parameters should be sufficient. See below for the available `@` placeholders.
``RESOLVE_PKG_COMPONENTS_TMPLVAR <varname>``
    Optional. The content of this variable is used to create the topologically sorted list of components to load. It must create a list variable ``requested_components`` listing these component names. See `Resolving Package Components`_ below for more details. If the argument missing or empty the unchanged component list received through ``find_package()`` is used.
``PROLOG_TMPLVAR <varname>``
    The content of this variable is inserted at the very beginning of the generated package config file. `@` placeholders are supported. See below for details.
``EPILOG_TMPLVAR <varname>``
    The content of this variable is inserted at the very end of the generated package config file. `@` placeholders are supported. See below for details.
``IMPORTER_MIN_CMAKE_VERSION <version>``
    Minimum required CMake version for the user of this exported package. Mostly useful in combination with a custom template or ``RESOLVE_PKG_COMPONENTS`` code.

Parameters for configuring the version info file:

``NO_VERSION`` disables the creation of the version info file and ignores all other versioning arguments.

The other parameters control if and how a *<package>-config-version.cmake* file is created. The parameters work the same as described for the function ``prac_write_package_version_file()`` in *Versioning.cmake*. `See there <prac_write_package_version_file()_>`_ for more details.

Output variable:

``<EXPORT-name>_EXPORT_DIR``
    Installation directory of the package config files relative to ``CMAKE_INSTALL_PREFIX``.

Placeholders available in ``TMPLVAR`` snippets:

``@export_name@`` (variable-like)
    Export name in its original spelling, taken from the ``EXPORT`` argument, ``PROJECT_NAMESPACE`` VARIABLE or ``PROJECT_NAME`` variable.
``@export_name_lower@`` (variable-like)
    Export name converted to lowercase.
``@export_dir@`` (variable-like)
    Directory where the export sets and package config CMake files are installed relative to ``CMAKE_INSTALL_PREFIX``.
``@fail_if_no_components_requested@`` (function-like)
    Aborts package import if the ``find_package()`` command does not specify any components.

Additional placeholders available in the template file:

``@importer_min_cmake_version@`` (variable-like)
    Minimum CMake version the user must run. Empty if no ``IMPORTER_MIN_CMAKE_VERSION`` argument was given and Prac does not need a minimum version for other reasons.
``@maybe_check_importer_min_cmake_version@`` (function-like)
    Checks the user’s CMake version and aborts package import if it is too old. Empty if ``@importer_min_cmake_version@`` is empty.
``@resolve_components@``, ``@maybe_resolve_components@`` (function-like)
    Takes the component list of the ``find_package()`` call and builds the ordered list of components to load. See the ``RESOLVE_PKG_COMPONENTS_TMPLVAR`` parameter and `Resolving Package Components`_ section below. The ``maybe_`` variant has the same content if components were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@fail_if_components_missing@``, ``@maybe_fail_if_components_missing@`` (function-like)
    Aborts package import if any of the requested components do not exist. The ``maybe_`` variant has the same content if components were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@load_components@``, ``@maybe_load_components@`` (function-like)
    Includes the CMake files defining the targets for each requested component. The ``maybe_`` variant has the same content if components were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@load_top_level_targets@``, ``@maybe_load_top_level_targets@`` (function-like)
    Includes the CMake file defining the top-level targets. The ``maybe_`` variant has the same content if top-level targets were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@maybe_import_dependencies@`` (function-like)
    Finds and imports dependencies registered with ``prac_find_package_and_export()`` for the current package – both top-level and requested components. Empty if the project has no dependencies.
``@prolog@``, ``@epilog@``
    The code related to the arguments ``PROLOG_TMPLVAR`` or ``EPILOG_TMPLVAR``. Empty if the respective argument was not used.

Resolving Package Components
````````````````````````````
Components may have dependencies between each other. Take Qt as an example. ``Qt::Gui`` depends on ``Qt::Core````. When you call ``find_package(Qt COMPONENTS Gui)`` you want ``Qt::Core`` to be loaded automatically. After all it’s Qt’s job to know its internal dependencies, not yours. Additionally the two components must be loaded in the correct order. ``Qt::Core`` must come first because loading ``Qt::Gui`` depends on the core targets existing already.

Prac has no way of figuring out such internal dependencies by itself. That’s why the ``RESOLVE_PKG_COMPONENTS_TMPLVAR`` parameter exists. It is expected to contain CMake code building the full list of components to load and sort that list topologically so that the components are loaded in the correct order.
]]
function(prac_install_config_package)
    # ---------- argument parsing and validation ----------
    set(switches
        NO_NAMESPACE NO_VERSION ARCH_INDEPENDENT
    )
    set(one_value_args
        EXPORT NAMESPACE
        TEMPLATE_FILE IMPORTER_MIN_CMAKE_VERSION RESOLVE_PKG_COMPONENTS_TMPLVAR PROLOG_TMPLVAR EPILOG_TMPLVAR
        VERSION COMPATIBILITY
    )
    set(multi_value_args)
    cmake_parse_arguments(PARSE_ARGV 0 "" "${switches}" "${one_value_args}" "${multi_value_args}")

    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()
    if (_KEYWORDS_MISSING_VALUES)
        message(FATAL_ERROR "Keyword arguments given without a value: ${_KEYWORDS_MISSING_VALUES}")
    endif()
    if (_NAMESPACE AND _NO_NAMESPACE)
        message(
            FATAL_ERROR
            "Arguments NAMESPACE and NO_NAMESPACE cannot be used at the same time."
        )
    endif()
    if ("${_NAMESPACE}" STREQUAL "::")
        message(FATAL_ERROR "NAMESPACE consists of only a doulbe colon `::`.")
    endif()

    # ---------- define common placeholders and function return value ----------

    pracprivate_define_export_name()

    if ("${PRAC_INSTALL_EXPORTROOTDIR}" STREQUAL "")
        pracprivate_define_export_root_dir(_export_root)
        set(export_dir "${_export_root}/${export_name}")
    else()
        set(export_dir "${PRAC_INSTALL_EXPORTROOTDIR}/${export_name}")
    endif()
    set(${export_name}_EXPORT_DIR "${export_dir}" PARENT_SCOPE)  # function’s return value

    set(_toplevel_targets_filename "${export_name_lower}-targets.cmake")

    # ----------

    pracprivate_instpkgcfg_prepare_template_snippets()
    pracprivate_instpkgcfg_install_export_sets()

    if (NOT _NO_VERSION)
        pracprivate_instpkgcfg_install_version_file()
    endif()
endfunction()
macro(pracprivate_instpkgcfg_prepare_template_snippets)
    set(maybe_load_top_level_targets)
    set(maybe_resolve_components)
    set(maybe_fail_if_components_missing)
    set(maybe_load_components)
    set(maybe_import_dependencies)
    set(importer_min_cmake_version)
    set(maybe_check_importer_min_cmake_version)

    # --- toplevel/components setup and validation

    string(CONFIGURE
        "${pracprivate_fail_if_no_components_requested_snippet}"
        fail_if_no_components_requested
        @ONLY
    )

    get_property(_has_toplevel GLOBAL PROPERTY pracprivate_install_pkgtoplevel_${export_name})
    get_property(_comp_names GLOBAL PROPERTY pracprivate_install_pkgcomps_${export_name})

    if (_comp_names)
        set(_has_components TRUE)
    else()
        set(_has_components FALSE)
    endif()


    if (_has_toplevel OR _TEMPLATE_FILE)
        set(load_top_level_targets
            "include(\"\${CMAKE_CURRENT_LIST_DIR}/${_toplevel_targets_filename}\")")
    endif()
    if (_has_toplevel)
        set(maybe_load_top_level_targets "${load_top_level_targets}")
    endif()


    if (_has_components OR _TEMPLATE_FILE)
        if (_RESOLVE_PKG_COMPONENTS_TMPLVAR)
            string(CONFIGURE "${${_RESOLVE_PKG_COMPONENTS_TMPLVAR}}" resolve_components @ONLY)
        else()
            set(resolve_components "${pracprivate_resolve_components_default_snippet}")
        endif()

        string(CONFIGURE "${pracprivate_check_comps_exist_snippet}" fail_if_components_missing @ONLY)
        string(CONFIGURE "${pracprivate_load_comps_snippet}" load_components @ONLY)
    endif()
    if (_has_components)
        set(maybe_resolve_components "${resolve_components}")
        set(maybe_fail_if_components_missing "${fail_if_components_missing}")
        set(maybe_load_components "${load_components}")
    endif()

    # --- find_dependency wrappers and finding project-wide 3rd party dependencies

    # global deps
    get_property(_dep_calls GLOBAL PROPERTY pracprivate_logged_dependency_calls_${export_name})
    if (_dep_calls)
        list(JOIN _dep_calls "" _dep_calls)
        string(APPEND maybe_import_dependencies "# global dependencies\n${_dep_calls}")
    endif()

    # component deps
    set(_has_comp_deps FALSE)
    foreach (comp IN LISTS _comp_names)
        get_property(
            _cdep_calls GLOBAL PROPERTY pracprivate_logged_dependency_calls_${export_name}_${comp}
        )
        if (_cdep_calls)
            set(_has_comp_deps TRUE)
            list(JOIN _cdep_calls "    " _cdep_calls)
            string(APPEND maybe_import_dependencies
"# component `${comp}` dependencies
if (\"${comp}\" IN_LIST requested_components)
    ${_cdep_calls}endif()
")
        endif()
    endforeach()

    if (maybe_import_dependencies)
        string(CONFIGURE "${pracprivate_finddep_wrapper_snippet}" pracprivate_finddep_wrapper_configured @ONLY)
        string(PREPEND maybe_import_dependencies "${pracprivate_finddep_wrapper_configured}")
    endif()

    # --- minimum CMake version required for importing projects

    if (_has_comp_deps AND ("${_IMPORTER_MIN_CMAKE_VERSION}" VERSION_LESS "3.3"))
        # comp specific deps use `if(IN_LIST)` -> available since 3.3
        set(_IMPORTER_MIN_CMAKE_VERSION "3.3")
    endif()
    if (_IMPORTER_MIN_CMAKE_VERSION)
        set(importer_min_cmake_version "${_IMPORTER_MIN_CMAKE_VERSION}")
        string(CONFIGURE "${pracprivate_check_version_snippet}" maybe_check_importer_min_cmake_version @ONLY)
    endif()

    # --- prolog and epilog

    if (_PROLOG_TMPLVAR)
        string(CONFIGURE "${${_PROLOG_TMPLVAR}}" prolog @ONLY)
    else()
        set(prolog)
    endif()
    if (_EPILOG_TMPLVAR)
        string(CONFIGURE "${${_EPILOG_TMPLVAR}}" epilog @ONLY)
    else()
        set(epilog)
    endif()
endmacro()
macro(pracprivate_instpkgcfg_install_export_sets)
    if (_NO_NAMESPACE)
        set(_namespace_arg)
    elseif ("${_NAMESPACE}" STREQUAL "")
        set(_namespace_arg NAMESPACE ${export_name}::)
    else()
        string(REPLACE "::" "" _NAMESPACE "${_NAMESPACE}")
        set(_namespace_arg NAMESPACE "${_NAMESPACE}::")
    endif()

    # components’ targets files
    foreach (comp IN LISTS _comp_names)
        get_property(_more_args GLOBAL PROPERTY pracprivate_install_instexpargs_${export_name}_${comp})
        install(
            EXPORT "${export_name}_${comp}"
            DESTINATION "${export_dir}"
            ${_namespace_arg}
            FILE "${export_name_lower}-component-${comp}.cmake"
            ${_more_args}
        )
    endforeach()

    # toplevel targets file
    if (_has_toplevel)
        get_property(_more_args GLOBAL PROPERTY pracprivate_install_instexpargs_${export_name})
        install(
            EXPORT "${export_name}"
            DESTINATION "${export_dir}"
            ${_namespace_arg}
            FILE "${_toplevel_targets_filename}"
            ${_more_args}
        )
    endif()

    # main package config file (entry point for user’s find_package() call)
    pracprivate_set_if_empty(_TEMPLATE_FILE "${pracprivateInstallation_here_dir}/Installation.Config.cmake.in")
    set(_config_gen_filepath "${CMAKE_CURRENT_BINARY_DIR}/${export_name_lower}-config.cmake")

    configure_file("${_TEMPLATE_FILE}" "${_config_gen_filepath}" @ONLY)
    install(FILES "${_config_gen_filepath}" DESTINATION "${export_dir}")
endmacro()
macro(pracprivate_instpkgcfg_install_version_file)
    set(version_filename "${export_name_lower}-config-version.cmake")
    pracprivate_set_if_empty(_COMPATIBILITY SemanticVersion)

    set(arch_arg)
    if (_ARCH_INDEPENDENT)
        set(arch_arg ARCH_INDEPENDENT)
    endif()

    set(version_arg)
    if (NOT "${_VERSION}" STREQUAL "")
        set(version_arg VERSION "${_VERSION}")
    endif()

    prac_write_package_version_file(
        "${version_filename}"
        ${version_arg}
        COMPATIBILITY ${_COMPATIBILITY}
        ${arch_arg}
    )

    install(
        FILES "${CMAKE_CURRENT_BINARY_DIR}/${version_filename}"
        DESTINATION "${export_dir}"
    )
endmacro()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


macro(pracprivate_define_export_root_dir outvar)
    if ("${CMAKE_INSTALL_LIBDIR}" STREQUAL "")
        set(${outvar} "lib/cmake")
    else()
        set(${outvar} "${CMAKE_INSTALL_LIBDIR}/cmake")
    endif()
endmacro()


#[[
Input variables:
    _EXPORT
    PROJECT_NAMESPACE
    PROJECT_NAME
Output variables:
    export_name
    export_name_lower
]]
macro(pracprivate_define_export_name)
    if (NOT "${_EXPORT}" STREQUAL "")
        set(export_name "${_EXPORT}")
    elseif (NOT "${PROJECT_NAMESPACE}" STREQUAL "")
        set(export_name "${PROJECT_NAMESPACE}")
    elseif (NOT "${PROJECT_NAME}" STREQUAL "")
        set(export_name "${PROJECT_NAME}")
    else()
        message(
            FATAL_ERROR
            "Cannot determine name of the export set. "
            "All of EXPORT argument, PROJECT_NAMESPACE and PROJECT_NAME variables are empty."
        )
    endif()

    string(TOLOWER "${export_name}" export_name_lower)
endmacro()


# Sets the variable ``var_name`` to ``value`` if it is currently empty or undefined.
# Does nothing otherwise.
macro(pracprivate_set_if_empty var_name value)
    if ("${${var_name}}" STREQUAL "")
        set(${var_name} ${value})
    endif()
endmacro()


set(pracprivate_check_version_snippet [[
if (CMAKE_VERSION VERSION_LESS @importer_min_cmake_version@)
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "@export_name@ requires CMake @importer_min_cmake_version@ or later. "
        "You are running version ${CMAKE_VERSION}."
    )
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()
]])


set(pracprivate_resolve_components_default_snippet [[
set(requested_components ${${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS})
]])

set(pracprivate_fail_if_no_components_requested_snippet [[
if ("${requested_components}" STREQUAL "")
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "Failed to import @export_name@. No COMPONENTS given.")
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()
]])

set(pracprivate_check_comps_exist_snippet [[
set(missing_comps)
foreach (comp IN LISTS requested_components)
    if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_${comp})
        if (NOT EXISTS "${CMAKE_CURRENT_LIST_DIR}/@export_name_lower@-component-${comp}.cmake")
            set(missing_comps "${missing_comps} ${comp}")
        endif()
    endif()
endforeach()
if (missing_comps)
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "@export_name@ is missing required components:${missing_comps}")
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()
unset(missing_comps)
]])

set(pracprivate_load_comps_snippet [[
foreach (comp IN LISTS requested_components)
    include("${CMAKE_CURRENT_LIST_DIR}/@export_name_lower@-component-${comp}.cmake" OPTIONAL)
endforeach()
]])

set(pracprivate_finddep_wrapper_snippet [[
# In CMake versions older than 3.15 `find_dependency()` does nothing if the `<dep>_FOUND`
# variable is true. That can lead to components of a dependency not being checked when
# different `find_dependency()` calls request different components of the same package.
# For example: When first requesting Boost filesystem and then requesting Boost asio, asio will
# not be checked and not be imported.
# Bugtracker issue: https://gitlab.kitware.com/cmake/cmake/issues/17583
if (CMAKE_VERSION VERSION_LESS 3.15)
    # older CMake version : reimpl of find_dependency() without the flawed optimization
    macro(prac_find_dependency dep_name)
        set(pracprivate_managed_args)
        if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
            list(APPEND pracprivate_managed_args REQUIRED)
        endif()
        if (${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY)
            list(APPEND pracprivate_managed_args QUIET)
        endif()

        find_package(${dep_name} ${ARGN} ${pracprivate_managed_args})
        unset(pracprivate_managed_args)

        if (NOT ${dep_name}_FOUND)
            set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
                "Finding @export_name@ failed because its dependency ${dep_name} could not be found.")
            set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
            return()
        endif()
    endmacro()
else()
    # recent enough CMake version where find_dependency() can be used directly
    include(CMakeFindDependencyMacro)
    macro(prac_find_dependency)
        find_dependency(${ARGV})
    endmacro()
endif()
]])
