#ifndef MY_GUARD
#define MY_GUARD

#define MY_PREFIX_VERSION_MAJOR 1
#define MY_PREFIX_VERSION_MINOR 2
#define MY_PREFIX_VERSION_PATCH 3
#define MY_PREFIX_VERSION_PRE ""
#define MY_PREFIX_VERSION_META ""

// major.minor.patch
#define MY_PREFIX_VERSION "1.2.3"

// major.minor.patch-pre (-pre is optional)
#define MY_PREFIX_VERSION_FULL "1.2.3"

// major.minor.patch-pre+meta (-pre and +meta are optional)
#define MY_PREFIX_VERSION_FULL_META "1.2.3"

#endif  // MY_GUARD
