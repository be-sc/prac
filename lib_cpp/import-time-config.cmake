
# Handling user config for the `prac` target.
if (PRAC_UNPREFIXED_MACROS)
    target_compile_definitions(prac INTERFACE PRAC_UNPREFIXED_MACROS)
endif()


if (PRAC_ASSERTS_ALWAYS_ON AND PRAC_ASSERTS_ALWAYS_OFF)
    message(
        FATAL_ERROR
        "PRAC_ASSERTS_ALWAYS_ON and PRAC_ASSERTS_ALWAYS_OFF cannot be ON at the same time."
    )
endif()
if (PRAC_ASSERTS_ALWAYS_ON)
    target_compile_definitions(prac INTERFACE PRAC_ASSERTS_ALWAYS_ON)
endif()
if (PRAC_ASSERTS_ALWAYS_OFF)
    target_compile_definitions(prac INTERFACE PRAC_ASSERTS_ALWAYS_OFF)
endif()


if (PRAC_FMTLIB AND PRAC_FMTLIB_HEADERONLY)
    message(
        FATAL_ERROR
        "PRAC_FMTLIB and PRAC_FMTLIB_HEADERONLY cannot be ON at the same time."
    )
endif()
if (PRAC_FMTLIB)
    set(pracprivate_fmt_tgt fmt::fmt)
elseif(PRAC_FMTLIB_HEADERONLY)
    set(pracprivate_fmt_tgt fmt::fmt-header-only)
endif()
if (pracprivate_fmt_tgt)
    if (NOT TARGET ${pracprivate_fmt_tgt})
        message(
            FATAL_ERROR
            "Requested fmtlib support for Prac, but no target `${pracprivate_fmt_tgt}` is available. "
            "You must call `find_package(fmt)` before `find_package(prac)` or `add_subdirectory(prac)`."
        )
    endif()

    target_link_libraries(prac INTERFACE ${pracprivate_fmt_tgt})
    target_compile_definitions(prac INTERFACE PRAC_FMTLIB)
    unset(pracprivate_fmt_tgt)
endif()

