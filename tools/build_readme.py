#!/usr/bin/env python3

import re
from enum import Enum
from pathlib import Path
from typing import List, Tuple

ROOT_DIR = Path(__file__).resolve().parent.parent
CMAKELIB_DIR: Path = ROOT_DIR / 'lib_cmake' / 'prac'
CPP_HEADER_DIR: Path = ROOT_DIR / 'lib_cpp' / 'include' / 'prac'
README_FILE: Path = ROOT_DIR / 'README.rst'


class Snippets:
    def __init__(self) -> None:
        self.file_name: str = ''
        self.file_comment: List[str] = []
        self.entity_comments: List[Tuple[str, List[str]]] = []

    def is_empty(self) -> bool:
        return not self.file_comment and not self.entity_comments


def main() -> None:
    cmake_filelist = [f for f in CMAKELIB_DIR.glob('*.cmake') if not f.name.startswith('_')]
    cmake_filelist.sort()
    all_cmake_snippets: List[Snippets] = []

    for file in cmake_filelist:
        file_snippets = collect_cmake_snippets(file)
        if not file_snippets.is_empty():
            file_snippets.file_name = file.name
            all_cmake_snippets.append(file_snippets)

    cpp_filelist = list(CPP_HEADER_DIR.glob('*.hpp'))
    cpp_filelist.extend(CPP_HEADER_DIR.glob('*.h'))
    cpp_filelist.sort()
    all_cpp_snippets: List[Snippets] = []

    for file in cpp_filelist:
        doc_snippet = collect_cpp_doc_comment(file)
        if not doc_snippet.is_empty():
            doc_snippet.file_name = file.name
            all_cpp_snippets.append(doc_snippet)

    readme_top_content, readme_cpp_api_intro = read_manual_part_of_readme()
    cmake_docs, cpp_docs = generate_documentation(all_cmake_snippets, all_cpp_snippets)

    write_readme(readme_top_content, readme_cpp_api_intro, cmake_docs, cpp_docs)


def collect_cmake_snippets(file: Path) -> Snippets:
    with file.open('r', encoding='utf-8') as fp:
        content = fp.readlines()

    snippets = Snippets()
    collect_cmake_start_of_file_comment(content, snippets)
    collect_cmake_entity_snippets(file, content, snippets)
    return snippets


def collect_cmake_start_of_file_comment(content: List[str], snippets: Snippets) -> None:
    class Parsing(Enum):
        before_main_comment = 0
        in_main_comment = 1

    state = Parsing.before_main_comment

    for line in content:
        if state == Parsing.before_main_comment:
            if line.startswith('#[===['):
                state = Parsing.in_main_comment
        elif state == Parsing.in_main_comment:
            if line.startswith(']===]'):
                break
            else:
                snippets.file_comment.append(line)
        else:
            raise ValueError(f'Unexpected enumerator: {state}')


def collect_cmake_entity_snippets(file: Path, content: List[str], snippets: Snippets) -> None:
    class Parsing(Enum):
        not_interesting = 0
        comment = 1
        entity_name = 2

    entity_re = re.compile(r'^\s*(?:function|macro)\((\w+)\b')
    current_comment = []
    state = Parsing.not_interesting

    for line_num, line in enumerate(content, start=1):
        if state == Parsing.not_interesting:
            if line.startswith('#[[.rst:'):
                state = Parsing.comment
        elif state == Parsing.comment:
            if line.startswith(']]'):  # end of comment reached
                state = Parsing.entity_name
            else:  # interesting comment line
                current_comment.append(line)
        elif state == Parsing.entity_name:
            match = entity_re.match(line)
            if match is None:
                raise RuntimeError(f'{file}:{line_num}: Expected function/macro declaration.')
            snippets.entity_comments.append((match.group(1) + '()', current_comment))
            current_comment = []
            state = Parsing.not_interesting
        else:
            raise ValueError('Unexpected enumerator.')

    if current_comment:
        raise RuntimeError(f'{file}: Unfinished comment: {current_comment}')


def collect_cpp_doc_comment(file: Path) -> Snippets:
    class Parsing(Enum):
        before_main_comment = 0
        in_main_comment = 1

    with file.open('r', encoding='utf-8') as fp:
        content = fp.readlines()

    state = Parsing.before_main_comment
    doc_snippet = Snippets()

    for line in content:
        if state == Parsing.before_main_comment:
            if line.startswith('/**'):
                state = Parsing.in_main_comment
        elif state == Parsing.in_main_comment:
            if line.startswith('*/'):
                break
            else:
                doc_snippet.file_comment.append(line)
        else:
            raise ValueError(f'Unexpected enumerator: {state}')

    return doc_snippet


def generate_documentation(
        all_cmake_snippets: List[Snippets], all_cpp_snippets: List[Snippets]
) -> Tuple[List[str], List[str]]:
    cmake_docs = []

    for file_snippets in all_cmake_snippets:
        file_snippets: Snippets
        cmake_docs.append(f'\n{file_snippets.file_name}\n{"=" * len(file_snippets.file_name)}\n')

        if file_snippets.file_comment:
            cmake_docs.append('\n')
            cmake_docs.extend(file_snippets.file_comment)
            cmake_docs.append('\n')

        for entity_name, comment in file_snippets.entity_comments:
            cmake_docs.append(f'\n{entity_name}\n{"-" * len(entity_name)}\n')
            cmake_docs.extend(comment)
            cmake_docs.append('\n')

    cpp_docs = []

    for file_snippets in all_cpp_snippets:
        file_snippets: Snippets
        cpp_docs.append(f'\n{file_snippets.file_name}\n{"=" * len(file_snippets.file_name)}\n\n')
        cpp_docs.extend(file_snippets.file_comment)
        cpp_docs.append('\n')

    return cmake_docs, cpp_docs


def read_manual_part_of_readme() -> Tuple[List[str], List[str]]:
    class Parsing(Enum):
        top_content = 0
        cpp_api_intro = 1
        generated_cmake = 2

    top_content = []
    cpp_api_intro = []
    state = Parsing.top_content

    with README_FILE.open('r', encoding='utf-8') as fp:
        line = fp.readline()

        while line:
            if state == Parsing.top_content:
                top_content.append(line)
                if line.startswith('.. @@generated-cmake-start@@'):
                    state = Parsing.generated_cmake
            elif state == Parsing.generated_cmake:
                if line.startswith('.. @@generated-cmake-end@@'):
                    cpp_api_intro.append(line)
                    state = Parsing.cpp_api_intro
            elif state == Parsing.cpp_api_intro:
                cpp_api_intro.append(line)
                if line.startswith('.. @@generated-cpp@@'):
                    break
            else:
                raise ValueError('Unexpected enumerator.')

            line = fp.readline()

    return top_content, cpp_api_intro


def write_readme(
    top_content: List[str], cpp_api_intro: List[str], cmake_docs: List[str], cpp_docs: List[str]
) -> None:
    with README_FILE.open('w', encoding='utf-8', newline='\n') as fp:
        for line in top_content:
            fp.write(line)
        for line in cmake_docs:
            fp.write(line)
        for line in cpp_api_intro:
            fp.write(line)
        for line in cpp_docs:
            fp.write(line)


if __name__ == '__main__':
    main()
