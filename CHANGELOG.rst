Prac Changelog
##############


Version 1.0.0 (2022-08-19)
==========================

Renamed the project from PPBoil to Prac. Reworked, tweaked and cleaned up the whole thing. What’s mentioned below are just the highlights.


lib_cmake
---------
* Removed ``enable_strict_cxx_checks()``. The compiler warning flags setup is too project specific to put it into a general library.

* Reworked installation support completely. Trying to force all aspects of installation into a single convenience function wasn’t a good idea. It works for simple cases but breaks down quickly for non-trivial setups. I realized there’s a good reason for all the incarnations of CMake’s ``install()`` command.


lib_cpp
-------
* Removed ``sysinfo_special.h``. CMake is the better tool to provide its functionality.
* Replaced the bare bones scope guard in *finally.hpp* with Alexandrescu-style scope guards in *scope_guards.hpp*.



PPBoil Version 5.1.0 (2021-07-25)
=================================

lib_cmake:
---------
All changes were to the semantic versioning support in exported projects.

* added support for CMake 3.19 version ranges in ``find_package()``
* fixed: ``find_package()`` without a version resulted in every version to be rejected
* fixed: The architecture check for architecture dependent packages failed with a misleading error message when ``find_package()`` was called before ``project()``.
* fixed: For pre-release versions ``PACKAGE_VERSION_EXACT`` was set as normal instead of never setting it.



PPBoil Version 5.0.0 (2021-01-16)
=================================

lib_cmake
---------
* ``enable_strict_cxx_checks()``

  * enabled a few more warnings
  * compiler version where a warning was introduced is now respected

* ``install_package()`` got an option to return the export directory path.


lib_cpp
-------
* added ``Finally`` scope guard
* introduced common config for disabling unprefixed names and replaced ``PPBOIL_ASSERTIONS_UNPREFIXED_OFF`` with ``PPBOIL_UNPREFIXED_OFF``


other
-----
* Ppboil’s own CMake project now has an ``install`` target that installs both the c++ library and the CMake modules.
* Added packaging support for Arch Linux.



PPBoil Version 4.0.0 (2020-02-04)
=================================

First polished release of both the CMake library and the C++ library.
