export_name=custom_template
export_name_lower=custom_template
export_dir=lib/cmake/custom_template
fail_if_no_components_requested=if ("${requested_components}" STREQUAL "")
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "Failed to import custom_template. No COMPONENTS given.")
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()

importer_min_cmake_version=3.20
