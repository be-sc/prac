#ifndef MYLIB_API_DEFINES
#define MYLIB_API_DEFINES

#if defined(custom_STATIC_LIB_DEFINE)
#   define custom_API_DEFINE
#   define custom_INTERNAL_DEFINE
#else
#   if defined(MYLIB_BUILDING_LIB)
#       define custom_API_DEFINE __attribute__((visibility("default")))
#   else
#       define custom_API_DEFINE __attribute__((visibility("default")))
#   endif

#   define custom_INTERNAL_DEFINE __attribute__((visibility("hidden")))
#endif

#define custom_DEPRECATED_DEFINE __attribute__((deprecated))
#define custom_DEPRECATED_MSG_DEFINE __attribute__((deprecated(msg)))

#define custom_DEPRECATED_API_DEFINE custom_API_DEFINE custom_DEPRECATED_DEFINE
#define custom_DEPRECATED_API_MSG_DEFINE(msg) custom_API_DEFINE custom_DEPRECATED_MSG_DEFINE(msg)

#define custom_DEPRECATED_INTERNAL_DEFINE custom_INTERNAL_DEFINE custom_DEPRECATED_DEFINE
#define custom_DEPRECATED_INTERNAL_MSG_DEFINE(msg) custom_INTERNAL_DEFINE custom_DEPRECATED_MSG_DEFINE(msg)

#endif  // MYLIB_API_DEFINES
