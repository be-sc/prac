# Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#[===[.rst:
Provides utility functions for versioning including semantic versioning support.
]===]
include_guard(GLOBAL)

set(pracprivateVersioning_here_dir "${CMAKE_CURRENT_LIST_DIR}")
include(CMakePackageConfigHelpers)
include("${pracprivateVersioning_here_dir}/_private_parts.cmake")

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Sets a semantic version number for the current project. Intended as a replacement for
``project(VERSION)`` which lacks semver support. Accordingly creates very similar output
variables in the caller’s scope. ::

    prac_project_semver(<semver>)

The function takes a single parameter, a valid semantic version number according to the `SemVer 2.0.0 specification <https://semver.org/>`_. The version follows this pattern::

    major.minor.patch-pre+meta

The first three components (the “version triple”) must be present. Each component of the triple must be a non-negative integer without leading zeros. The pre and meta components are optional.

Output variables:

``<project_name>_VERSION_MAJOR`` and ``PROJECT_VERSION_MAJOR``
    Contains the major version number.
``<project_name>_VERSION_MINOR`` and ``PROJECT_VERSION_MINOR``
    Contains the minor version number.
``<project_name>_VERSION_PATCH`` and ``PROJECT_VERSION_PATCH``
    Contains the patch version number.
``<project_name>_VERSION_PRE`` and ``PROJECT_VERSION_PRE``
    Contains the pre-release info without the leading dash. Empty if not present.
``<project_name>_VERSION_META`` and ``PROJECT_VERSION_META``
    Contains the version metadata without the leading plus sign. Empty if not present.
``<project_name>_VERSION`` and ``PROJECT_VERSION``
    Contains the dot-separated major, minor and patch version (e.g. *1.0.0*). Does not contain pre-release or metadata info to remain compatible with the corresponding all-integer variable created by the ``project()`` command.
``<project_name>_VERSION_FULL`` and ``PROJECT_VERSION_FULL``
    Version up to and including the pre-release component, but without the metadata.
``<project_name>_VERSION_FULL_META`` and ``PROJECT_VERSION_FULL_META``
    Complete version number including the pre-release and metadata components.
``CMAKE_PROJECT_VERSION``
    If ``prac_project_semver()`` is called from the top-level CMakeLists.txt, set to the same value as ``PROJECT_VERSION``. Mimics the built-in ``project()`` command’s behaviour introduced in CMake 3.12.

Some of the version variables might contain the same string. For example, if no pre-release component is present ``PROJECT_VERSION`` and ``PROJECT_VERSION_FULL`` are identical.

Semantic versioning has no equivalent of the numeric ``TWEAK`` version component supported by the ``project()`` command. ``prac_project_semver()`` does not try to mimic that component.

The pre-release and metadata components are not fully validated because they have no meaning for version compatibility checks. If you want to validate further yourself you may assume that both components only contain valid characters.
]]
function(prac_project_semver semver)
    prac_parse_semantic_version("${semver}")

    if (PRAC_VER_ERROR)
        message(FATAL_ERROR "`${semver}` is not a valid version. ${PRAC_VER_ERROR}")
    endif()

    set(${PROJECT_NAME}_VERSION_MAJOR     "${PRAC_VER_MAJOR}"  PARENT_SCOPE)
    set(PROJECT_VERSION_MAJOR             "${PRAC_VER_MAJOR}"  PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_MINOR     "${PRAC_VER_MINOR}"  PARENT_SCOPE)
    set(PROJECT_VERSION_MINOR             "${PRAC_VER_MINOR}"  PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_PATCH     "${PRAC_VER_PATCH}"  PARENT_SCOPE)
    set(PROJECT_VERSION_PATCH             "${PRAC_VER_PATCH}"  PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_PRE       "${PRAC_VER_PRE}"    PARENT_SCOPE)
    set(PROJECT_VERSION_PRE               "${PRAC_VER_PRE}"    PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_META      "${PRAC_VER_META}"   PARENT_SCOPE)
    set(PROJECT_VERSION_META              "${PRAC_VER_META}"   PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION           "${PRAC_VER_TRIPLE}" PARENT_SCOPE)
    set(PROJECT_VERSION                   "${PRAC_VER_TRIPLE}" PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_FULL      "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}" PARENT_SCOPE)
    set(PROJECT_VERSION_FULL              "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}" PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_FULL_META "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}${PRAC_VER_META_PLUS}" PARENT_SCOPE)
    set(PROJECT_VERSION_FULL_META         "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}${PRAC_VER_META_PLUS}" PARENT_SCOPE)

    set(${PROJECT_NAME}_VERSION_TWEAK PARENT_SCOPE "")
    set(PROJECT_VERSION_TWEAK PARENT_SCOPE "")

    get_directory_property(parent_dir PARENT_DIRECTORY)
    if (NOT parent_dir)  # this is the top-level project
        set(CMAKE_PROJECT_VERSION "${PRAC_VER_TRIPLE}" PARENT_SCOPE)
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Generates a *<package>-config-version.cmake* file for an installed package.

This function is the same as ``write_basic_package_version_file()`` with added semantic versioning support. Its signature is almost the same::

    prac_write_package_version_file(
        <output_file>
        [COMPATIBILITY {SemanticVersion|<built-in_specifier>}]
        [VERSION {<semantic_version>|<major.minor.patch>}]
        [ARCH_INDEPENDENT]
    )

If ``COMPATIBILITY`` is not ``SemanticVersion`` all arguments are simply forwarded to ``write_basic_package_version_file()``. As a result it depends on the CMake version what the list of ``<built-in_specifier>`` is and whether ``ARCH_INDEPENDENT`` is supported. ``COMPATIBILITY`` defaults to ``SemanticVersion``.

For ``SemanticVersion`` mode the following applies regardless of the CMake version:

``<output_file>``
    Path to the output file. Relative paths are interpreted as relative to ``CMAKE_CURRENT_BINARY_DIR``.
``VERSION``
    A valid semantic version string. The version of the installed package is reported exactly as specified with this argument. However, as per semver rules, only major and minor version are relevant for the compatibility check. Defaults to the current project’s version. Project versions set with both ``package(VERSION)`` or ``prac_project_semver()`` are supported.
``ARCH_INDEPENDENT``
    Works similar to the built-in variant. It is intended for header-only libraries or other packages without binaries. If set the architecture of the installed package is not checked. Otherwise the architectures must match exactly. For example, an installed package built for Linux x86_64 can only be used when building for Linux x86_64 as well.
]]
function(prac_write_package_version_file out_file)
    if (NOT out_file)
        message(FATAL_ERROR "Path to output file is missing.")
    endif()

    cmake_parse_arguments(PARSE_ARGV 1 "" "ARCH_INDEPENDENT" "VERSION;COMPATIBILITY" "")

    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown keyword arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()

    if (NOT _COMPATIBILITY)
        set(_COMPATIBILITY "SemanticVersion")
    endif()

    if (NOT "${_COMPATIBILITY}" STREQUAL "SemanticVersion")
        write_basic_package_version_file(${ARGV})
        return()
    endif()

    # User requested SemanticVersion mode.

    if (DEFINED _VERSION)
        prac_parse_semantic_version("${_VERSION}")

        if (PRAC_VER_ERROR)
            message(FATAL_ERROR "${PRAC_VER_ERROR} [Version: ${_VERSION}]")
        endif()

        set(ver_full_meta "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}${PRAC_VER_META_PLUS}")
    else()
        if (${PROJECT_NAME}_VERSION_FULL_META)  # we got a semver
            set(PRAC_VER_MAJOR     "${${PROJECT_NAME}_VERSION_MAJOR}")
            set(PRAC_VER_MINOR     "${${PROJECT_NAME}_VERSION_MINOR}")
            set(PRAC_VER_PATCH     "${${PROJECT_NAME}_VERSION_PATCH}")
            set(PRAC_VER_PRE       "${${PROJECT_NAME}_VERSION_PRE}")
            set(ver_full_meta "${${PROJECT_NAME}_VERSION_FULL_META}")
            set(PRAC_VER_TRIPLE    "${${PROJECT_NAME}_VERSION}")
        else()
            if (NOT (DEFINED ${PROJECT_NAME}_VERSION_MAJOR AND
                     DEFINED ${PROJECT_NAME}_VERSION_MINOR)
            )
                message(
                    FATAL_ERROR
                    "Project major and/or minor version not set. These two components are required "
                    "for the version compatibility check.")
            endif()

            set(PRAC_VER_MAJOR "${${PROJECT_NAME}_VERSION_MAJOR}")
            set(PRAC_VER_MINOR "${${PROJECT_NAME}_VERSION_MINOR}")

            if (${PROJECT_NAME}_VERSION_PATCH)
                set(PRAC_VER_PATCH "${${PROJECT_NAME}_VERSION_PATCH}")
            else()
                set(PRAC_VER_PATCH 0)
            endif()

            set(ver_full_meta "${PRAC_VER_MAJOR}.${PRAC_VER_MINOR}.${PRAC_VER_PATCH}")

            # Tweak version might be present, but user requested SemanticVersion mode. Tweak has
            # no meaning there and can be ignored.
        endif()
    endif()

    if (PRAC_VER_PRE)
        set(_EXACT_VERSION_CHECK "
        # About the exact version check:
        # This is a pre-release version. find_package() does not allow requesting a pre-release
        # version. Thus we cannot reliably check for an exact version match. We are conservative
        # and never claim an exact match.")
    else()
        set(_EXACT_VERSION_CHECK "
        if (PACKAGE_FIND_VERSION_PATCH EQUAL ${PRAC_VER_PATCH})
            set(PACKAGE_VERSION_EXACT TRUE)
        endif()")
    endif()

    if (_ARCH_INDEPENDENT)
        set(matching_arch_check "")
    else()
        set(arch_built_for "${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}")
        string(CONFIGURE "${pracprivate_pkgver_archdep_snippet}" matching_arch_check @ONLY)
    endif()

    configure_file(
        "${pracprivateVersioning_here_dir}/Versioning.ConfigVersion.cmake.in"
        "${out_file}"
        @ONLY
    )
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Creates a header file with version information usable from both C++ and C. ::

    prac_write_version_header(
        <output_file>
        [VERSION {<semver>|<major.minor.patch.tweak>}]
        [MACRO_PREFIX <name>]
        [PRAGMA_ONCE | INCLUDE_GUARD_NAME <name>]
        [PREAMBLE <content>]
        [APPENDIX <content>]
    )

Parameters:

``<output_file>``
    Path to the output file. Relative paths are interpreted as relative to ``CMAKE_CURRENT_BINARY_DIR``.
``VERSION``
    Either a semantic version string or a numeric version with 1–4 components. Defaults to the current project’s version as set by either ``project(VERSION)`` or ``prac_project_semver()``.
``MACRO_PREFIX``
    Prefix for macro names. Automatically converted to upper case. Defaults to to the ``PROJECT_NAME``.
``PRAGMA_ONCE`` and ``INCLUDE_GUARD_NAME <name>``
    Configures the include guard style used by the header file. Defaults to an include guard macro with a name derived from ``MACRO_PREFIX``.
``PREAMBLE <content>``
    Inserted verbatim at the very beginning of the generated header, i.e. *before* the include guard. Intended for adding a start-of-file comment.
``APPENDIX <content>``
    Inserted verbatim at the end of the generated header *before* closing the include guard. Any custom code you want to have in the header should go here.
]]
function(prac_write_version_header output_file)
    if (NOT output_file)
        message(FATAL_ERROR "Path to output file is missing.")
    endif()

    set(switches PRAGMA_ONCE)
    set(one_value_args VERSION MACRO_PREFIX INCLUDE_GUARD_NAME PREAMBLE APPENDIX)
    cmake_parse_arguments(PARSE_ARGV 1 "" "${switches}" "${one_value_args}" "")

    if (_UNPARSED_ARGUMENTS)
        message(FATAL_ERROR "Unknown keyword arguments given: ${_UNPARSED_ARGUMENTS}")
    endif()

    if (NOT _MACRO_PREFIX)
        set(_MACRO_PREFIX "${PROJECT_NAME}")
    endif()
    string(MAKE_C_IDENTIFIER "${_MACRO_PREFIX}" _MACRO_PREFIX)
    string(TOUPPER "${_MACRO_PREFIX}" _MACRO_PREFIX)

    if (NOT _INCLUDE_GUARD_NAME)
        set(_INCLUDE_GUARD_NAME "${_MACRO_PREFIX}_VERSION_INFO")
    endif()

    # Ensure clean slate for text formatting in output file.
    set(guard_start "")
    set(ver_defines "")
    set(guard_end "")

    pracprivate_version_header_prepare_version()
    pracprivate_make_guard_snippet(${_PRAGMA_ONCE} "${_INCLUDE_GUARD_NAME}")

    if (_PREAMBLE)
        string(APPEND _PREAMBLE "\n")
    endif()

    if (_APPENDIX)
        string(APPEND _APPENDIX "\n")
    endif()

    pracprivate_append_c_version_num("_MAJOR" PRAC_VER_MAJOR ver_defines)
    pracprivate_append_c_version_num("_MINOR" PRAC_VER_MINOR ver_defines)
    pracprivate_append_c_version_num("_PATCH" PRAC_VER_PATCH ver_defines)

    if (PRAC_VER_IS_SEMANTIC)
        pracprivate_append_c_version_str("_PRE" PRAC_VER_PRE ver_defines)
        pracprivate_append_c_version_str("_META" PRAC_VER_META ver_defines)
        string(APPEND ver_defines "\n// major.minor.patch\n")
        pracprivate_append_c_version_str("" PRAC_VER_TRIPLE ver_defines)
        string(APPEND ver_defines "\n// major.minor.patch-pre (-pre is optional)\n")
        pracprivate_append_c_version_str("_FULL" ver_full ver_defines)
        string(APPEND ver_defines "\n// major.minor.patch-pre+meta (-pre and +meta are optional)\n")
        pracprivate_append_c_version_str("_FULL_META" ver_full_meta ver_defines)
    else()
        pracprivate_append_c_version_num("_TWEAK" PRAC_VER_TWEAK ver_defines)
        pracprivate_append_c_version_str("" ver_full ver_defines)
    endif()

    configure_file(
        "${pracprivateVersioning_here_dir}/Versioning.version.header.in"
        "${output_file}"
        @ONLY
    )
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Parses a semantic version string. ::

    prac_parse_semantic_version(<semver_string>)

The function takes a single parameter, a valid semantic version number according to the `SemVer 2.0.0 specification <https://semver.org/>`_. The version follows this pattern::

    major.minor.patch-pre+meta

The first three components (the “version triple”) must be present. Each component of the triple must be a non-negative integer without leading zeros. The pre and meta components are optional.

Outputs:

``PRAC_VER_ERROR``
    Empty if parsing succeeds. Otherwise contains an error message. In the error case the state of the other output variables is undefined. Do not rely on them.
``PRAC_VER_MAJOR``, ``PRAC_VER_MINOR``, ``PRAC_VER_PATCH``
    The major, minor and patch version numbers.
``PRAC_VER_TRIPLE``
    The major, minor and patch version combined into an *x.y.z* version triple string.
``PRAC_VER_PRE``, ``PRAC_VER_PRE_DASH``
    The pre-release component excluding and including the leading dash. Both variables are empty if the version has no pre-release component.
``PRAC_VER_META``, ``PRAC_VER_META_PLUS``
    The metadata component excluding and including the leading plus sign. Both variables are empty if the version has no metadata component.
]]
function(prac_parse_semantic_version semver_string)
    string(STRIP "${semver_string}" semver_string)

    # Cleanup calling scope. Some of these variables get overwritten below.
    set(PRAC_VER_ERROR "" PARENT_SCOPE)
    set(PRAC_VER_MAJOR "" PARENT_SCOPE)
    set(PRAC_VER_MINOR "" PARENT_SCOPE)
    set(PRAC_VER_PATCH "" PARENT_SCOPE)
    set(PRAC_VER_PRE "" PARENT_SCOPE)
    set(PRAC_VER_PRE_DASH "" PARENT_SCOPE)
    set(PRAC_VER_META "" PARENT_SCOPE)
    set(PRAC_VER_META_PLUS "" PARENT_SCOPE)

    if (semver_string STREQUAL "")
        set(PRAC_VER_ERROR "Version cannot be emtpy." PARENT_SCOPE)
        return()
    endif()

    # Regex for extracting pre and meta components. Version triple is only roughly checked
    # for invalid characters.
    if (semver_string MATCHES "^([0-9.]+)(-[0-9A-Za-z.\-]+)?([+][0-9A-Za-z.\-]+)?$")
        set(ver_triple "${CMAKE_MATCH_1}")

        if (CMAKE_MATCH_2)
            string(SUBSTRING "${CMAKE_MATCH_2}" 1 -1 PRAC_VER_PRE)
            set(PRAC_VER_PRE "${PRAC_VER_PRE}" PARENT_SCOPE)
            set(PRAC_VER_PRE_DASH "${CMAKE_MATCH_2}" PARENT_SCOPE)
        endif()

        if (CMAKE_MATCH_3)
            string(SUBSTRING "${CMAKE_MATCH_3}" 1 -1 PRAC_VER_META)
            set(PRAC_VER_META "${PRAC_VER_META}" PARENT_SCOPE)
            set(PRAC_VER_META_PLUS "${CMAKE_MATCH_3}" PARENT_SCOPE)
        endif()
    endif()

    # The version triple can be parsed as a dotted version.
    pracprivate_parse_dotted_version("${ver_triple}")

    if (NOT "${ver_error}" STREQUAL "")
        set(PRAC_VER_ERROR "${ver_error}" PARENT_SCOPE)
        return()
    endif()

    list(LENGTH ver_components comp_count)

    if (NOT comp_count EQUAL 3)
        set(PRAC_VER_ERROR
            "Version starts with ${comp_count} numeric components. Expected 3 (major.minor.patch)."
            PARENT_SCOPE
        )
        return()
    endif()

    set(PRAC_VER_TRIPLE "${ver_triple}" PARENT_SCOPE)

    list(GET ver_components 0 comp)
    set(PRAC_VER_MAJOR  "${comp}"  PARENT_SCOPE)

    list(GET ver_components 1 comp)
    set(PRAC_VER_MINOR  "${comp}"  PARENT_SCOPE)

    list(GET ver_components 2 comp)
    set(PRAC_VER_PATCH  "${comp}"  PARENT_SCOPE)
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Parses a numeric version number with up to four components: *major.minor.patch.tweak*. ::

    prac_parse_numeric_version(<version>)

Each component must be a non-negative integer without leading zeros. The major version must be present, the other three are optional.

Outputs:

``PRAC_VER_ERROR``
    Empty if parsing succeeds. Otherwise contains an error message. In the error case the state of the other output variables is undefined. Do not rely on them.
``PRAC_VER_MAJOR``, ``PRAC_VER_MINOR``, ``PRAC_VER_PATCH``, ``PRAC_VER_TWEAK``
    The four components of the version. Missing components are empty.
``PRAC_VER_LEN``
    The number of components contained in the version (1–4).
]]
function(prac_parse_numeric_version version)
    string(STRIP "${version}" version)

    # Cleanup calling scope. Some of these variables get overwritten below.
    set(PRAC_VER_ERROR "" PARENT_SCOPE)
    set(PRAC_VER_LEN 0 PARENT_SCOPE)
    set(PRAC_VER_MAJOR "" PARENT_SCOPE)
    set(PRAC_VER_MINOR "" PARENT_SCOPE)
    set(PRAC_VER_PATCH "" PARENT_SCOPE)
    set(PRAC_VER_TWEAK "" PARENT_SCOPE)

    if (version STREQUAL "")
        set(PRAC_VER_ERROR "Version cannot be emtpy." PARENT_SCOPE)
        return()
    endif()

    pracprivate_parse_dotted_version("${version}")

    if (NOT "${ver_error}" STREQUAL "")
        set(PRAC_VER_ERROR "${ver_error}" PARENT_SCOPE)
        return()
    endif()

    list(LENGTH ver_components comp_count)

    if (comp_count GREATER 4)
        set(PRAC_VER_ERROR "${comp_count} components found. Expected between 1 and 4." PARENT_SCOPE)
        return()
    endif()

    set(PRAC_VER_LEN ${comp_count} PARENT_SCOPE)

    list(GET ver_components 0 comp)
    set(PRAC_VER_MAJOR "${comp}" PARENT_SCOPE)

    if (comp_count GREATER 1)
        list(GET ver_components 1 comp)
        set(PRAC_VER_MINOR "${comp}" PARENT_SCOPE)
    endif()

    if (comp_count GREATER 2)
        list(GET ver_components 2 comp)
        set(PRAC_VER_PATCH "${comp}" PARENT_SCOPE)
    endif()

    if (comp_count GREATER 3)
        list(GET ver_components 3 comp)
        set(PRAC_VER_TWEAK "${comp}" PARENT_SCOPE)
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Parses any kind of supported version string. ::

    prac_parse_any_version(<semver_or_numeric_version>)

First tries to parse the string as a semantic version. If that fails tries parsing as a numeric version.

Outputs:

``PRAC_VER_ERROR``
    Empty if parsing succeeds. Otherwise contains the combined error messages from both parsing attempts. In the error case the state of the other output variables is undefined. Do not rely on them.
``PRAC_VER_IS_SEMANTIC``
    `TRUE` if the version was successfully parsed as a semantic version, `FALSE` otherwise. Semantic parsing is attempted first. A version valid in both schemes will be detected as semantic.
*parsing result*
    Several output variables depending on which parsing attempt succeeded. See ``prac_parse_semantic_version()`` and ``prac_parse_numeric_version()`` for details.
]]
macro(prac_parse_any_version version)
    prac_parse_semantic_version("${version}")

    if (PRAC_VER_ERROR)
        set(pracprivate_ver_error_sem "${PRAC_VER_ERROR}")
        unset(PRAC_VER_ERROR)
        set(PRAC_VER_IS_SEMANTIC FALSE)

        prac_parse_numeric_version("${version}")

        if (PRAC_VER_ERROR)
            set(PRAC_VER_ERROR
                "Unrecognized version format. Not a semantic version because: '${pracprivate_ver_error_sem}'. Not a numeric version because: '${PRAC_VER_ERROR}'."
            )
        endif()

        unset(pracprivate_ver_error_sem)
    else()
        set(PRAC_VER_IS_SEMANTIC TRUE)
    endif()
endmacro()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


# Parses up numeric version components, separated by dots.
# Expects a whitespace-stripped, non-empty version.
# Output:
#   ver_error (empty on success)
#   ver_components (empty on error)
macro(pracprivate_parse_dotted_version version)
    # CMake’s regex syntax does not support non-capturing groups, resulting in really weird
    # capture groups. For simplicity we only use the regex to do validation. Getting at the
    # version components without the dots is simpler with a simple string split.
    if ("${version}" MATCHES "^(0|([1-9][0-9]*))(\.(0|([1-9][0-9]*)))*$")
        set(ver_error)
        string(REPLACE "." ";" ver_components "${version}")
    else()
        set(ver_error
            "Numeric version components must be non-negative integers without leading zeros."
        )
    endif()
endmacro()


# requires: _VERSION
# sets: all ver_*
macro(pracprivate_version_header_prepare_version)
    if (NOT "${_VERSION}" STREQUAL "")
        prac_parse_any_version("${_VERSION}")

        if (PRAC_VER_ERROR)
            message(FATAL_ERROR "${PRAC_VER_ERROR} [version: ${_VERSION}]")
        endif()

        if (PRAC_VER_IS_SEMANTIC)
            set(ver_full "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}")
            set(ver_full_meta "${PRAC_VER_TRIPLE}${PRAC_VER_PRE_DASH}${PRAC_VER_META_PLUS}")
        else()
            set(ver_full "${_VERSION}")
        endif()
    else()
        if ("${${PROJECT_NAME}_VERSION_MAJOR}" STREQUAL "")
            message(FATAL_ERROR "No project version defined. [project: ${PROJECT_NAME}]")
        endif()

        set(PRAC_VER_MAJOR "${${PROJECT_NAME}_VERSION_MAJOR}")
        set(PRAC_VER_MINOR "${${PROJECT_NAME}_VERSION_MINOR}")
        set(PRAC_VER_PATCH "${${PROJECT_NAME}_VERSION_PATCH}")
        set(PRAC_VER_TWEAK "${${PROJECT_NAME}_VERSION_TWEAK}")
        set(PRAC_VER_PRE   "${${PROJECT_NAME}_VERSION_PRE}")
        set(PRAC_VER_META  "${${PROJECT_NAME}_VERSION_META}")

        if (NOT "${${PROJECT_NAME}_VERSION_FULL_META}" STREQUAL "")  # we have a semver
            set(PRAC_VER_IS_SEMANTIC TRUE)
            set(PRAC_VER_TRIPLE "${${PROJECT_NAME}_VERSION}")
            set(ver_full "${${PROJECT_NAME}_VERSION_FULL}")
            set(ver_full_meta "${${PROJECT_NAME}_VERSION_FULL_META}")
        else()
            set(PRAC_VER_IS_SEMANTIC FALSE)
            set(ver_full "${${PROJECT_NAME}_VERSION}")
        endif()
    endif()
endmacro()


# helpers for prac_write_version_header(), NOT self-contained
# require: _MACRO_PREFIX
macro(pracprivate_append_c_version_num suffix version_varname out_varname)
    if (${version_varname} LESS 0)
        message(
            FATAL_ERROR
            "Version component ${suffix} cannot be negative. Got: ${${version_varname}}")
    endif()

    string(
        APPEND ${out_varname}
        "#define ${_MACRO_PREFIX}_VERSION${suffix} ${${version_varname}}\n"
    )
endmacro()

macro(pracprivate_append_c_version_str suffix version_varname out_varname)
    string(
        APPEND ${out_varname}
        "#define ${_MACRO_PREFIX}_VERSION${suffix} \"${${version_varname}}\"\n"
    )
endmacro()


set(pracprivate_pkgver_archdep_snippet [[
if (PACKAGE_VERSION_COMPATIBLE)
    if ((NOT DEFINED CMAKE_SYSTEM_NAME) OR (NOT DEFINED CMAKE_SYSTEM_PROCESSOR))
        message(
            FATAL_ERROR
            "Could not determine target architecture. CMAKE_SYSTEM_NAME and/or CMAKE_SYSTEM_PROCESSOR "
            "are not defined. Most likely you called `find_package(${CMAKE_FIND_PACKAGE_NAME})` "
            "before `project()`."
        )
    endif()

    set(arch_built_for "@arch_built_for@")
    set(find_arch "${CMAKE_SYSTEM_NAME}-${CMAKE_SYSTEM_PROCESSOR}")

    if (NOT arch_built_for STREQUAL "${find_arch}")
        string(APPEND PACKAGE_VERSION " ${arch_built_for} (incompatible target arch: ${find_arch})")
        set(PACKAGE_VERSION_UNSUITABLE TRUE)
    endif()
endif()
]])
