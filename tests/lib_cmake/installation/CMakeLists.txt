cmake_minimum_required(VERSION ${PRAC_CM_MINVER} FATAL_ERROR)

include("../test_framework.cmake")
include(prac/Configuration)
include(prac/Installation)

set(CMAKE_INSTALL_PREFIX "install")

project(testsuite LANGUAGES CXX)
prac_project_semver(1.2.3)

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

prac_add_namespaced_library(toplevel_lib SHARED "../dummy_source.cpp")
prac_add_namespaced_library(sublevel_lib SHARED "../dummy_source.cpp")

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

start_test("find_package() calls are registered for export")
    # Using the private macro to avoid the real find_package() calls.

    # default export and component
    pracprivate_register_find_dependency_call(Qt5 5.15 REQUIRED COMPONENTS Core Gui)
    pracprivate_register_find_dependency_call(Catch2 REQUIRED)
    get_property(top_calls GLOBAL PROPERTY pracprivate_logged_dependency_calls_testsuite)
    check_equal(
        top_calls
        "prac_find_dependency(Qt5 5.15 REQUIRED COMPONENTS Core Gui)\n;prac_find_dependency(Catch2 REQUIRED)\n"
        ${CMAKE_CURRENT_LIST_LINE}
    )

    # custom export and sub component
    pracprivate_register_find_dependency_call(EXPORT cat Qt5 5.15 REQUIRED COMPONENTS Core Gui)
    pracprivate_register_find_dependency_call(EXPORT cat PKG_COMPONENT paw Catch2 REQUIRED)
    get_property(top_calls GLOBAL PROPERTY pracprivate_logged_dependency_calls_cat)
    check_equal(
        top_calls
        "prac_find_dependency(Qt5 5.15 REQUIRED COMPONENTS Core Gui)\n"
        ${CMAKE_CURRENT_LIST_LINE}
    )
    get_property(sub_calls GLOBAL PROPERTY pracprivate_logged_dependency_calls_cat_paw)
    check_equal(
        sub_calls
        "prac_find_dependency(Catch2 REQUIRED)\n"
        ${CMAKE_CURRENT_LIST_LINE}
    )

    # cleanup registered dependencies
    set_property(GLOBAL PROPERTY pracprivate_logged_dependency_calls_testsuite)
    set_property(GLOBAL PROPERTY pracprivate_logged_dependency_calls_cat)
    set_property(GLOBAL PROPERTY pracprivate_logged_dependency_calls_cat_paw)
    unset(top_calls)
    unset(sub_calls)
end_test()

start_test("pkgcfg file: minimal, default export set")
    # pkgcfg only contains the include for ...-targets.cmake
    prac_install_payload(TARGETS testsuite_toplevel_lib)
    prac_install_config_package(NO_VERSION)
    check_files_equal(
        "${CMAKE_CURRENT_BINARY_DIR}/testsuite-config.cmake"
        "${CMAKE_CURRENT_LIST_DIR}/expected/minimal.cmake"
        ${CMAKE_CURRENT_LIST_LINE}
    )
end_test()

start_test("pkgcfg file: prolog, epilog, min version, no components")
    set(prolog "# Prolog for @export_name@")
    set(epilog "# Epilog for @export_name_lower@")
    prac_install_payload(
        TARGETS testsuite_toplevel_lib
        EXPORT Prolog_Epilog_Version
    )
    prac_install_config_package(
        EXPORT Prolog_Epilog_Version
        PROLOG_TMPLVAR prolog
        EPILOG_TMPLVAR epilog
        IMPORTER_MIN_CMAKE_VERSION 3.20
        NO_VERSION
    )
    check_files_equal(
        "${CMAKE_CURRENT_BINARY_DIR}/prolog_epilog_version-config.cmake"
        "${CMAKE_CURRENT_LIST_DIR}/expected/prolog_epilog_version.cmake"
        ${CMAKE_CURRENT_LIST_LINE}
    )
end_test()

start_test("pkgcfg file: custom template")
    prac_install_payload(
        TARGETS testsuite_toplevel_lib
        EXPORT custom_template
    )
    prac_install_config_package(
        EXPORT custom_template
        TEMPLATE_FILE "config.cmake.in"
        IMPORTER_MIN_CMAKE_VERSION 3.20
        NO_VERSION
    )
    check_files_equal(
        "${CMAKE_CURRENT_BINARY_DIR}/custom_template-config.cmake"
        "${CMAKE_CURRENT_LIST_DIR}/expected/custom_template.cmake"
        ${CMAKE_CURRENT_LIST_LINE}
    )
end_test()

start_test("pkgcfg file: components with deps")
    set(prolog "# Prolog for @export_name@")
    set(epilog "# Epilog for @export_name_lower@")
    set(_exp_name comps_with_deps)
    set(_comp_name subcomponent)
    pracprivate_register_find_dependency_call(EXPORT ${_exp_name} Qt5 5.15 REQUIRED COMPONENTS Core Gui)
    pracprivate_register_find_dependency_call(EXPORT ${_exp_name} PKG_COMPONENT ${_comp_name} Catch2 REQUIRED)

    prac_install_payload(
        TARGETS testsuite_toplevel_lib
        EXPORT ${_exp_name}
    )
    prac_install_payload(
        TARGETS testsuite_sublevel_lib
        EXPORT ${_exp_name}
        PKG_COMPONENT ${_comp_name}
    )
    prac_install_config_package(
        EXPORT ${_exp_name}
        PROLOG_TMPLVAR prolog
        EPILOG_TMPLVAR epilog
        IMPORTER_MIN_CMAKE_VERSION 3.20
        NO_VERSION
    )
    check_files_equal(
        "${CMAKE_CURRENT_BINARY_DIR}/${_exp_name}-config.cmake"
        "${CMAKE_CURRENT_LIST_DIR}/expected/${_exp_name}.cmake"
        ${CMAKE_CURRENT_LIST_LINE}
    )
end_test()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

abort_if_testsuite_failed()
