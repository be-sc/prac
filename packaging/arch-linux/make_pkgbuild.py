#!/usr/bin/python3

import argparse
import hashlib
import os
import pathlib
import re
import shutil
import subprocess
import sys
from typing import Optional, Sequence

PROJECT_BASENAME = 'prac'

# path relative to the project root dir
PKGBUILD_TMPL_FILEPATH = 'packaging/arch-linux/PKGBUILD.template'

# regex to find version number in CMakeLists.txt. Line looks like this:
# prac_project_semver(<semantic version number>)
VERSION_RE = re.compile(rf'^prac_project_semver\(([^)]+)\)$')

# regex to find min required CMake version in CMakeLists.txt. Line looks like this:
# cmake_minimum_required(VERSION x.y FATAL_ERROR)
CMAKE_MIN_VERSION_RE = re.compile(
    rf'^cmake_minimum_required\(VERSION ([0-9]+(?:.[0-9]+)+) FATAL_ERROR\)$')

# path to the CMakeLists.txt containing the version number
# path relative to the project root dir
VERSION_CMLISTS_FILEPATH = 'CMakeLists.txt'


def make_arch_installer(projroot_dir: pathlib.Path, dest_dir: pathlib.Path) -> None:
    cmakelists = read_cmakelists(projroot_dir)
    version = extract_project_version_from_cmakelists(cmakelists, projroot_dir)
    cmake_min_version = extract_cmake_min_version_from_cmakelists(cmakelists, projroot_dir)

    basename = f'{PROJECT_BASENAME}-{version}'
    arch_filepath = dest_dir / (basename + '.tar.gz')
    pkgb_filepath = dest_dir / 'PKGBUILD'

    os.makedirs(dest_dir, exist_ok=True)

    subprocess.run(
        [
            'git', 'archive',
            '--format', 'tar.gz',
            '--prefix', f'{basename}/',
            '--output', str(arch_filepath),
            'HEAD',
        ],
        check=True,
        cwd=str(projroot_dir),
    )

    shutil.copy(
        os.path.join(projroot_dir, PKGBUILD_TMPL_FILEPATH),
        pkgb_filepath,
    )

    format_pkgbuild(pkgb_filepath, version, cmake_min_version, calc_archive_checksum(arch_filepath))


def read_cmakelists(projroot_dir: pathlib.Path) -> Sequence[str]:
    cmlists_file = projroot_dir / VERSION_CMLISTS_FILEPATH
    with open(cmlists_file, mode='r', encoding='utf-8') as fp:
        return fp.readlines()


def extract_project_version_from_cmakelists(
    cmakelists: Sequence[str], projroot_dir: pathlib.Path
) -> str:
    version: Optional[str] = None

    for line in cmakelists:
        match = re.match(VERSION_RE, line)
        if match is not None:
            version = match[1]
            break

    if version is None:
        raise RuntimeError('Project version not found in CMakeLists.txt')

    # strip metadata
    meta_start = version.find('+')
    if meta_start != -1:
        version = version[0:meta_start]

    # Replace hyphen. Makepkg does not accept versions containing hyphens.
    return version.replace('-', '_')


def extract_cmake_min_version_from_cmakelists(
    cmakelists: Sequence[str], projroot_dir: pathlib.Path
) -> str:
    for line in cmakelists:
        match = re.match(CMAKE_MIN_VERSION_RE, line)
        if match is not None:
            return match[1]

    raise RuntimeError('CMake min version not found in CMakeLists.txt.')


def calc_archive_checksum(arch_filepath: pathlib.Path) -> str:
    with open(arch_filepath, mode='rb') as fp:
        return hashlib.sha256(fp.read()).hexdigest()


def format_pkgbuild(
    pkgb_filepath: pathlib.Path, version: str, cmake_min_version: str, checksum: str
) -> None:
    with open(pkgb_filepath, mode='r', encoding='utf-8') as fp:
        lines = fp.readlines()

    for idx, line in enumerate(lines):
        lines[idx] = (
            line
            .replace('<<<pkgver>>>', version)
            .replace('<<<checksum>>>', checksum)
            .replace('<<<cmakeversion>>>', cmake_min_version)
        )

    with open(pkgb_filepath, mode='w', encoding='utf-8') as fp:
        for line in lines:
            fp.write(line)


def main(cli_args: Sequence[str]) -> None:
    parser = argparse.ArgumentParser(description=
        'Creates PKGBUILD and sources archive in the current directory.'
        'The archive is built from the last Git commit on the current branch.'
        'Uncommitted changes are ignored.'
    )
    parser.parse_args(cli_args)
    make_arch_installer(pathlib.Path(__file__).parent.resolve().parent.parent, pathlib.Path.cwd())


if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        print('Aborted manually.', file=sys.stderr)
        sys.exit(1)
