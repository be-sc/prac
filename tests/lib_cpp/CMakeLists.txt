add_executable(prac_testsuite
    "catch2_main.cpp"
    "scope_guards_tests.cpp"
)

target_link_libraries(prac_testsuite
    PRIVATE
        prac
        Catch2::Catch2
)

add_test(
    NAME
        prac_testsuite
    COMMAND
        prac_testsuite
)
