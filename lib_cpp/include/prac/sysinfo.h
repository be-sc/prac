// Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
/**
A pragmatic C++ and C header for compile-time detection of the target platform and some target platform features.

It is designed to be minimal. It only uses the preprocessor, has no further dependencies and does not include any other headers. Also it does not try to handle every obscure platform you can think of. Instead it focuses on the major platforms (Linux, BSD, Android, Apple, Windows) on usual hardware (x86, ARM), either 64bit or 32bit.

All macros are either defined without a value or not defined at all.

Main macros to identify the target OS/platform. Only one of these macros is defined at a given time:

==========================  =======================================
macro                       defined when compiling for
==========================  =======================================
``PRAC_SYSINFO_ANDROID``    Android
``PRAC_SYSINFO_APPLE``      any Darwin-based Apple OS
``PRAC_SYSINFO_BSD``        FreeBSD, NetBSD, OpenBSD, DragonFly BSD
``PRAC_SYSINFO_CYGWIN``     Cygwin
``PRAC_SYSINFO_LINUX``      Linux
``PRAC_SYSINFO_WINDOWS``    Microsoft Windows
==========================  =======================================

Additional platform info, defined in addition to the main macros above:

==========================  =====================================================
macro                       defined when compiling for
==========================  =====================================================
``PRAC_SYSINFO_MINGW``      MinGW or MinGW-w64, in addition to ``WINDOWS``
``PRAC_SYSINFO_UNIX``       a Unix-like system, applies to all except ``WINDOWS``
==========================  =====================================================

Macros to identify platform details:

===================================  =======================================================
macro                                defined if
===================================  =======================================================
``PRAC_SYSINFO_64BIT``               target platform is 64bit
``PRAC_SYSINFO_32BIT``               target platform is 32bit
``PRAC_SYSINFO_LITTLE_ENDIAN``       target platform has little endian byte order [1]
``PRAC_SYSINFO_BIG_ENDIAN``          target platform has big endian (network) byte order [1]
``PRAC_SYSINFO_RTTI``                C++ RTTI is enabled
``PRAC_SYSINFO_EXCEPTIONS``          C++ exceptions are enabled
===================================  =======================================================

+ [1] MSVC does not provide a preprocessor way to detect the endianness. Little endian is assumed because Windows only runs on little endian platforms. This does not affect Clang in MSVC ABI mode because it provides its normal endianness detection even in that mode.
*/
// Because of the macro heavy code clang-format is not useful here.
// clang-format off
#pragma once

/* Operating system and similar platform info */

#if defined(__ANDROID__)
    #define PRAC_SYSINFO_ANDROID
#elif defined(__linux__)
    #define PRAC_SYSINFO_LINUX
#elif defined(__APPLE__) && defined(__MACH__)
    #define PRAC_SYSINFO_APPLE
#elif defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__DragonFly__)
    #define PRAC_SYSINFO_BSD
#elif defined(__CYGWIN__) && !defined(_WIN32)
    #define PRAC_SYSINFO_CYGWIN
#elif defined(_WIN32)
    #define PRAC_SYSINFO_WINDOWS

    #if defined(__MINGW32__) || defined(__MINGW64__)
        #define PRAC_SYSINFO_MINGW
    #endif
#endif

#if defined(__unix__) || defined(__unix) || defined(PRAC_SYSINFO_APPLE)
    #define PRAC_SYSINFO_UNIX
#endif


/* Platform details */

#if (defined(__SIZEOF_POINTER__) && (__SIZEOF_POINTER__ == 8)) \
        || defined(_M_AMD64) || defined(_M_ARM64) || defined(_M_X64)
    #define PRAC_SYSINFO_64BIT
#else
    #define PRAC_SYSINFO_32BIT
#endif

#if defined(__BYTE_ORDER__)
    #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        #define PRAC_SYSINFO_LITTLE_ENDIAN
    #elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        #define PRAC_SYSINFO_BIG_ENDIAN
    #endif
#elif defined(_MSC_VER)
    #define PRAC_SYSINFO_LITTLE_ENDIAN
#endif

#if defined(_cpp_rtti) || defined(__GXX_RTTI) || defined(_CPPRTTI)
    #define PRAC_SYSINFO_RTTI
#endif

#if defined(__cpp_exceptions) || defined(__EXCEPTIONS) || defined(_CPPUNWIND)
    #define PRAC_SYSINFO_EXCEPTIONS
#endif
