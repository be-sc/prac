*************************
Prac: Project Accessories
*************************

This repo contains:

* a CMake utility library using modern CMake 3.x idioms
* a C++ utility library (partly C compatible)

The CMake and C++ parts can be used independently. If you build and install Prac via its own CMake project both libraries are part of that installation.


Licence
#######

Everything is released as `CC0/public domain`__.

__ https://creativecommons.org/share-your-work/public-domain/cc0/


Quick Overview
##############

CMake Utility Library
=====================

The *lib_cmake* directory contains a library of utility functions for a CMake 3.x project. Under *examples/cmake-project* you can find an example project using the library.

The library requires **at least CMake 3.14**. See the `CMake Library API`_ documentation below for more details. Here is an overview:

* Target configuration and related functions, such as detailed C++ and C compiler detection.
* Commands for configuring package installation, including support for config packages with components as knwon from larger projects like Boost or Qt.
* Configuration of shared library symbol visibility and associated macros (an alternative to *GenerateExportHeader*).
* Support for working with semantic versioning: parsing of semver strings, semver for exported CMake projects and generated version headers.


C++ Utility Library
===================

The *lib_cpp* directory contains a static library of utility functions for C++. Some of them are C compatible. The C++ parts require **at least C++17**. See the `C++ Library API`_ documentation below for more details. Here is an overview:

* Assertion macros with support for a custom message, including string formatting.
* Compile-time detection of the operating system, endianness, presence of exceptions, etc.



Integrating Prac Into Your Project
##################################

Building
========
You can build Prac in two ways.

You can build it **independently** through its own CMake build system. There are no build-time options to configure. Example::

    cd builddir
    cmake path/to/prac-root-dir -DCMAKE_BUILD_TYPE=Release
    cmake --build .
    cmake --install .

You can also build it as a **subproject** by calling ``add_subdirectory(path/to/prac-root-dir)`` in your own project’s CMake code.


Importing
=========
Import an **independent** installation as usual with ``find_package(prac)``.

* The C++ library is available as the CMake target ``prac``.
* Modules from the CMake library can be included with calls like ``include(prac/Installation)``.

When building as a **subproject** the ``prac`` C++ target is available after the ``add_subdirectory()`` call. You likely need the CMake library earlier, so you have to edit ``CMAKE_MODULE_PATH`` yourself or inclulde the CMake file through their path instead.


Import-time Configuration
=========================
Prac provides a few options for using the C++ library. All options can be configured as boolean variables in your project’s CMake code or as preprocessor defines. The names are the same.

* CMake: Set the variables to a true or false value before the ``find_package()`` or ``add_subdirectory()`` call. Undefined variables are considered false.
* Preprocessor: Configure the macros before including any Prac headers. Define all the variables you want to set to true without a value. Do not define the other ones.

Available options:

* header-specific options: `assert.hpp <assert-config_>`_
* library-wide options

  ``PRAC_UNPREFIXED_MACROS``
      By default all public macros are defined with the prefix ``PRAC_``. This option makes commonly used macros available without the prefix as well. See the header-specific documentation for details.


CMake Library Structure
=======================

If you want to only use parts of the CMake library, here is the dependency graph between the modules::

    _private_parts.cmake    Configuration.cmake
        ^   ^
        |   |
        |   +---------------+
        |                   |
    LibraryApi.cmake   Versioning.cmake
                            ^
                            |
                    Installation.cmake

    All.cmake (convenience wrapper, depends on all others)

The *.in* template files are not included in the graph. They are named after the module they belong to. Consider the module and its templates as one entity.



.. _CMake Library API:

CMake Library API
#################

.. @@generated-cmake-start@@

All.cmake
=========

Convenience module that includes all other Prac modules.


Configuration.cmake
===================

Provides functions for project and target configuration.


prac_detect_toplevel()
----------------------
Detects if the current *CMakeLists.txt* is the toplevel one or is used as a subproject. Equivalent to the ``PROJECT_IS_TOP_LEVEL`` variable introduced in CMake 3.21 but can be used before calling ``project()``. ::

    prac_detect_toplevel([<out_varname>])

If the ``<out_varname>`` is used it specifies the name of the output variable. If the function is called without any parameters the output variable is called ``IS_TOPLEVEL``.

Sets the output variable to ``TRUE`` if the current *CMakeLists.txt* is the top-level one, ``FALSE`` otherwise.


prac_project_namespace()
------------------------
Defines the namespace associated with the current project. Must be called after ``project()``. ::

    prac_project_namespace([<nsname>])

The namespace name may end in a double colon ``::`` which is removed from the output variables. It must not contain any other double colons. If ``<nsname>`` is empty or missing it defaults to the ``PROJECT_NAME``.

The final namespace name is stored in the variables ``PROJECT_NAMESPACE`` as well as ``<project-name>_NAMESPACE``.


prac_add_namespaced_library()
-----------------------------
Adds a library including double colon namespace handling. ::

    prac_add_namespaced_library(<libname> [NAMESPACE <ns>] [<add_library_args...>])

If you intend to use Prac’s extensions to CMake’s install functionality, prefer this command to create library targets. It is designed to configure the new target so it works seamlessly with the extensions.

Parameters:

``<libname>``
    The unqualified name of the library.
``NAMESPACE <ns>``
    The namespace of the library. Can be given with or without a trailing double colon (``::``). Must not contain any other double colons. If empty or missing defaults to the ``<project-name>_NAMESPACE`` and, if that is not defined, to the ``PROJECT_NAME``.
``<add_library_args...>``
    Any arguments supported by the built-in ``add_library()`` command except the library name itself. These arguments are passed verbatim to ``add_library()``.

Output:

* Adds a library target ``<ns>_<libname>``.
* Adds an alias library target ``<ns>::<libname>``.
* Sets the library’s ``EXPORT_NAME`` property to ``<libname>``.


prac_add_namespaced_executable()
--------------------------------
Adds an executable including double colon namespace handling. ::

    prac_add_namespaced_executable(<exename> [NAMESPACE <ns>] [<add_executable_args...>])

If you intend to use Prac’s extensions to CMake’s install functionality, prefer this command to create executable targets. It is designed to configure the new target so it works seamlessly with the extensions.

Parameters and output are equivalent to ``prac_add_namespaced_library()``.


prac_detect_cxx_and_c_compilers()
---------------------------------
Performs C++ and C compiler detection. This means:

* Detailed detection for the “big 3”: Clang, GCC, MSVC.
* Compilers that openly simulate GCC or MSVC via the ``CMAKE_<lang>_SIMULATE_ID`` variable are recognized as GCC compatible or MSVC compatible.
* Also recognizes the Intel compiler as GCC compatible on non-Windows platforms.

Usage::

    prac_detect_cxx_and_c_compilers()

Output variables for C++, set to either ``TRUE`` or ``FALSE``:

``PRAC_CXX_GCC_COMPAT``
    True if the compiler is GCC, or Clang in GCC-compatible mode, or another compiler reported as GCC compatible.
``PRAC_CXX_GCC``
    True if the compiler is the real GCC.
``PRAC_CXX_CLANG``
    True if the compiler is Clang, no matter in which mode.
``PRAC_CXX_MSVC_COMPAT``
    True if the compiler is MSVC, or Clang in MSVC-compatible command line mode (clang-cl), or
    another compiler reported as MSVC compatible.
``PRAC_CXX_MSVC``
    True if the compiler is the real MSVC.
``PRAC_CXX_COMPILER_DETECTED``
    True if any C++ compiler was detected, no matter which one.
``PRAC_CXX_COMPILER_PP_DEFINES``
    A list of preprocessor macro defines that can be used with ``target_compile_definitions()`` and accessed in the source code with ``#ifdef``. A macro is defined without a value for each of the above output variables set to ``TRUE``. For the other output variables no defines are created. The macro names are the same as the output variable names.

For C the same set of variables is defined with the prefix ``C_`` instead of ``CXX_``.

Calling this function multiple times is valid. For an already detected compiler the detection does not run again. Its variables remain unchanged.


prac_verify_cxx_compiler_detected()
-----------------------------------
Checks if a C++ compiler was detected with ``prac_detect_cxx_and_c_compilers()`` and aborts with a ``FATAL_ERROR`` if not. ::

    prac_verify_cxx_compiler_detected()


prac_verify_c_compiler_detected()
---------------------------------
Checks if a C compiler was detected with ``prac_detect_cxx_and_c_compilers()`` and aborts with a ``FATAL_ERROR`` if not. ::

    prac_verify_c_compiler_detected()


prac_target_cxx_standard()
--------------------------
Sets the required version of the C++ standard for a target. ::

    prac_target_cxx_standard(<target_name> {PUBLIC|PRIVATE|INTERFACE} <version> [EXTENSIONS])

Parameters:

``{PUBLIC|PRIVATE|INTERFACE}``
    the normal CMake scopes
``version``
    Accepted values are the same as for CMake’s ``CXX_STANDARD`` variable or the ``cxx_std_`` compile feature.
``EXTENSIONS``
    If given compiler extensions are allowed, otherwise they are prohibited.

The function handles both the classic triple of target properties (``CXX_STANDARD``, ``CXX_STANDARD_REQUIRED``, ``CXX_EXTENSIONS``) and the ``cxx_std_<version>`` compile feature as long as the CMake version supports it. The compile feature supports all scopes. Accordingly it is always configured for the given scope. The properties triple is private to the target. Following the usual CMake scope logic it is configured if ``PUBLIC`` or ``PRIVATE`` is given as a scope, but not touched for ``INTERFACE``.

Also the function takes some idiosyncrasies of MSVC++ into account:

* It always enables the alternative operator names mandated in [lex.digraph] in the standard, such as *not* instead of *!*. This is done either by force including the *iso646.h* header or by setting the */permissive-* option, depending on which is more appropriate for the given arguments.
* If the ``EXTENSIONS`` argument is missing and the compiler is at least MSVC++ 2017 it sets the */permissive-* switch to put the compiler into standard compliant mode.


prac_target_c_standard()
------------------------
Sets the required version of the C standard for a target. ::

    prac_target_c_standard(<target_name> {PUBLIC|PRIVATE|INTERFACE} <version> [EXTENSIONS])

Parameters:

``{PUBLIC|PRIVATE|INTERFACE}``
    the normal CMake scopes
``version``
    Accepted values are the same as for CMake’s ``C_STANDARD`` variable or the ``c_std_`` compile feature.
``EXTENSIONS``
    If given compiler extensions are allowed, otherwise they are prohibited.

The function handles both the classic triple of target properties (``C_STANDARD``, ``C_STANDARD_REQUIRED``, ``C_EXTENSIONS``) and the ``c_std_<version>`` compile feature as long as the CMake version supports it. The compile feature supports all scopes. Accordingly it is always configured for the given scope. The properties triple is private to the target. Following the usual CMake scope logic it is configured if ``PUBLIC`` or ``PRIVATE`` is given as a scope, but not touched for ``INTERFACE``.


prac_target_link_flags()
------------------------
Safely appends link flags to the existing link flags of the given target. ::

    prac_target_link_flags(<target_name> <flag1> ...)

The ``LINK_FLAGS`` target property is a space separated string. That does not fit CMake’s list handling semantics and makes appending by hand awkward and error prone. Also, we have ``target_compile_options()`` and friends. Having a similar function for the linker just makes sense.

Note: Since CMake 3.13 ``target_link_options()`` exists and should be preferred.


prac_target_symbol_visibility()
-------------------------------
Sets the symbol visibility for ``<target_name>`` which must be a C++ or C target. ::

    prac_target_symbol_visibility(<target_name> <visibility> [HIDDEN_INLINES])

Parameters:

``<visibility>``
    Symbol visibility mode. Pass the same value you would set the ``<LANG>_VISIBILITY_PRESET`` property to.
``HIDDEN_INLINES``
    If present configures the compiler to hide symbols of inline functions.

If you also want to create a header file with the usual set of export/API macros, consider using ``prac_configure_library_api()`` from the ``LibraryApi`` module instead.


Installation.cmake
==================

Provides functionality for automating target installation and package export. CMake requires quite a bit of work to get a full-featured installation working correctly.

For the exported config package this module uses the common practice of creating a top-level *<package>-config.cmake* file for custom code, which uses ``include()`` to pull in the main target config file created by ``install(EXPORT)``. That file is renamed to *<package>-targets.cmake*.

An often needed piece of custom code is automating finding transitive dependencies by placing the appropriate ``find_dependency()`` calls in the exported config package. The ``prac_find_package_and_export()`` function in this module provides a way to record ``find_package()`` calls relevant for installation.

About CMake’s two kinds of COMPONENT
------------------------------------

CMake has ``install(COMPONENT)`` and ``find_package(COMPONENTS)``. Those are not the same thing! If you have ``install(COMPONENT foo)`` in your CMake code that does not mean someone else can call ``find_package(COMPONENTS foo)`` later. Those are two different and completely separate kinds of component.

**Install components** give you control over which parts of a project to install. Different components usually have different purposes. Think of Linux distributions like Ubuntu that split up projects into a productive component – the one you install to use the software – and a development component – the one you install additionally if you want to use that software in programming.

**Package components** give you control over which parts of an already installed software you want to depend on in your own project. They are commonly used to structure large projects. Qt and Boost are well known examples. In contrast to install components CMake has no built-in support for package components other than the ``COMPONENTS`` parameter of ``find_package()``. The config package must handle such a component list by itself. CMake does not help. The extensions in this file do.

Look for the ``PKG_COMPONENT`` parameter in several functions to define the content of a set of package components. ``prac_install_config_package()`` generates a config package with code for handling those components during a ``find_package()`` call.


prac_find_package_and_export()
------------------------------
Calls ``find_package()`` with the given arguments and records the call for later use in an config package. ::

    prac_find_package_and_export(
        [EXPORT <name>]
        [PKG_COMPONENT <comp>]
        <find_package-args...>
    )

Parameters:

``EXPORT <name>``
    Optional name of the export set the ``find_package()`` call belongs to. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``PKG_COMPONENT <comp>``
    Optional name of a package component below the given export set. Defaults to the nameless top-level component.

Outputs:

Calls are logged to a global property and are automatically used by ``prac_install_config_package()`` when installing the export set as a config package.


prac_define_install_dirs()
--------------------------
Defines installation destination directories. Includes ``GNUInstallDirs`` to define the usual ones. Also defines additional ones. ::

    prac_define_install_dirs()

Additional install destinations:

``PRAC_INSTALL_DOCROOTDIR``
    Similar to ``CMAKE_INSTALL_DOCDIR`` but does not include the ``PROJECT_NAME`` as its last subdirectory. Default: *CMAKE_INSTALL_DATAROOTDIR/doc*.
``PRAC_INSTALL_EXPORTROOTDIR``
    Defines the project independent root directory for export sets and package config files. Default: *CMAKE_INSTALL_LIBDIR/cmake*.


prac_define_export_root_dir()
-----------------------------
Defines only the ``PRAC_INSTALL_EXPORTROOTDIR`` installation directory. See ``prac_define_install_dirs()`` for details about its location. ::

    prac_define_export_root_dir()


prac_install_payload()
----------------------
An extension to the built-in ``install(TARGETS)``. Installs and associates targets with an export set and optionally one of its package components. ::

    prac_install_payload(
        TARGETS <target>...
        [EXPORT <name>]
        [PKG_COMPONENT <comp>]
        [<install-TARGETS-args>...]
    )

Parameters:

``TARGETS <targets>...``
    A list of one or more existing targets.
``EXPORT <name>``
    Optional name of the export set the target will belong to. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``PKG_COMPONENT <comp>``
    Optional name of a package component below the given export set. Defaults to the nameless top-level component.
all other arguments
    passed verbatim ``install(TARGETS)``


prac_configure_config_package()
-------------------------------
Customizes the ``install(EXPORT)`` command for the given export set and optionally one of its package components. ::

    prac_configure_config_package(
        [EXPORT <name>]
        [PKG_COMPONENT <comp>]
        [<install-EXPORT-args>...]
    )

Must be called before the ``prac_install_config_package()`` for the same export set.

Parameters:

``EXPORT <name>``
    Optional name of the export set. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``PKG_COMPONENT <comp>``
    Optional name of a package component below the given export set. Defaults to the nameless top-level component.
all other arguments
    Recorded verbatim for later passing to ``install(EXPORT)``.


prac_install_config_package()
-----------------------------
Configures the content of a set of config package files and creates those files. ::

    prac_install_config_package(
        [EXPORT <name>]
        [NAMESPACE <ns> | NO_NAMESPACE]

        [TEMPLATE_FILE <filepath>]
        [RESOLVE_PKG_COMPONENTS_TMPLVAR <varname>]
        [PROLOG_TMPLVAR <varname]
        [EPILOG_TMPLVAR <varname>]
        [IMPORTER_MIN_CMAKE_VERSION <version>]

        [ # version configuration
          NO_VERSION | [
            [VERSION {<major.minor.patch>|<semantic_version>}]
            [COMPATIBILITY {<built-in_specifier>|SemanticVersion}]
            [ARCH_INDEPENDENT]
          ]
        ]
    )

Parameters for configuring the exported config package:

``EXPORT <name>``
    Optional name of the export set. Becomes the name of the exported config package. If empty, falls back to the ``PROJECT_NAMESPACE`` and then to the ``PROJECT_NAME``.
``NAMESPACE <ns>`` and ``NO_NAMESPACE``
    Custom namespace control. By default the exported targets are put into a namespace called the same as the export set. Disable that behaviour with ``NO_NAMESPACE`` or pick a custom one with ``NAMESPACE``. The custom name can be given with or without a trailing double colon (``::``). If needed Prac adds it automatically.
``TEMPLATE_FILE <filepath>``
    Path to the template file used to generate the package config file. Only needed under special circumstances. In most situations the default template in combination with the ``_TMPLVAR`` parameters should be sufficient. See below for the available `@` placeholders.
``RESOLVE_PKG_COMPONENTS_TMPLVAR <varname>``
    Optional. The content of this variable is used to create the topologically sorted list of components to load. It must create a list variable ``requested_components`` listing these component names. See `Resolving Package Components`_ below for more details. If the argument missing or empty the unchanged component list received through ``find_package()`` is used.
``PROLOG_TMPLVAR <varname>``
    The content of this variable is inserted at the very beginning of the generated package config file. `@` placeholders are supported. See below for details.
``EPILOG_TMPLVAR <varname>``
    The content of this variable is inserted at the very end of the generated package config file. `@` placeholders are supported. See below for details.
``IMPORTER_MIN_CMAKE_VERSION <version>``
    Minimum required CMake version for the user of this exported package. Mostly useful in combination with a custom template or ``RESOLVE_PKG_COMPONENTS`` code.

Parameters for configuring the version info file:

``NO_VERSION`` disables the creation of the version info file and ignores all other versioning arguments.

The other parameters control if and how a *<package>-config-version.cmake* file is created. The parameters work the same as described for the function ``prac_write_package_version_file()`` in *Versioning.cmake*. `See there <prac_write_package_version_file()_>`_ for more details.

Output variable:

``<EXPORT-name>_EXPORT_DIR``
    Installation directory of the package config files relative to ``CMAKE_INSTALL_PREFIX``.

Placeholders available in ``TMPLVAR`` snippets:

``@export_name@`` (variable-like)
    Export name in its original spelling, taken from the ``EXPORT`` argument, ``PROJECT_NAMESPACE`` VARIABLE or ``PROJECT_NAME`` variable.
``@export_name_lower@`` (variable-like)
    Export name converted to lowercase.
``@export_dir@`` (variable-like)
    Directory where the export sets and package config CMake files are installed relative to ``CMAKE_INSTALL_PREFIX``.
``@fail_if_no_components_requested@`` (function-like)
    Aborts package import if the ``find_package()`` command does not specify any components.

Additional placeholders available in the template file:

``@importer_min_cmake_version@`` (variable-like)
    Minimum CMake version the user must run. Empty if no ``IMPORTER_MIN_CMAKE_VERSION`` argument was given and Prac does not need a minimum version for other reasons.
``@maybe_check_importer_min_cmake_version@`` (function-like)
    Checks the user’s CMake version and aborts package import if it is too old. Empty if ``@importer_min_cmake_version@`` is empty.
``@resolve_components@``, ``@maybe_resolve_components@`` (function-like)
    Takes the component list of the ``find_package()`` call and builds the ordered list of components to load. See the ``RESOLVE_PKG_COMPONENTS_TMPLVAR`` parameter and `Resolving Package Components`_ section below. The ``maybe_`` variant has the same content if components were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@fail_if_components_missing@``, ``@maybe_fail_if_components_missing@`` (function-like)
    Aborts package import if any of the requested components do not exist. The ``maybe_`` variant has the same content if components were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@load_components@``, ``@maybe_load_components@`` (function-like)
    Includes the CMake files defining the targets for each requested component. The ``maybe_`` variant has the same content if components were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@load_top_level_targets@``, ``@maybe_load_top_level_targets@`` (function-like)
    Includes the CMake file defining the top-level targets. The ``maybe_`` variant has the same content if top-level targets were registrered for this package with ``prac_install_payload()``. It is empty otherwise.
``@maybe_import_dependencies@`` (function-like)
    Finds and imports dependencies registered with ``prac_find_package_and_export()`` for the current package – both top-level and requested components. Empty if the project has no dependencies.
``@prolog@``, ``@epilog@``
    The code related to the arguments ``PROLOG_TMPLVAR`` or ``EPILOG_TMPLVAR``. Empty if the respective argument was not used.

Resolving Package Components
````````````````````````````
Components may have dependencies between each other. Take Qt as an example. ``Qt::Gui`` depends on ``Qt::Core````. When you call ``find_package(Qt COMPONENTS Gui)`` you want ``Qt::Core`` to be loaded automatically. After all it’s Qt’s job to know its internal dependencies, not yours. Additionally the two components must be loaded in the correct order. ``Qt::Core`` must come first because loading ``Qt::Gui`` depends on the core targets existing already.

Prac has no way of figuring out such internal dependencies by itself. That’s why the ``RESOLVE_PKG_COMPONENTS_TMPLVAR`` parameter exists. It is expected to contain CMake code building the full list of components to load and sort that list topologically so that the components are loaded in the correct order.


LibraryApi.cmake
================

Provides functionality for defining the API of a library as source code annotations. Also configures the symbol visibility for shared libraries. Generates a header file with the appropriate preprocessor defines. Use *LibraryApi* as an alternative for the built-in *GenerateExportHeader*.

Some background on why ``prac_configure_library_api()`` is a complete reimplementation of ``generate_export_header()`` instead of a convenience wrapper:

*GenerateExportHeader* uses a default naming scheme that focuses on the technical aspects of the problem, namely exporting (or not) symbols from a shared library. For example, for a library *foo* the macro used for marking a symbol for export is ``FOO_EXPORT``.

The problem with this naming scheme is its level of abstraction. From a library author’s point of view the important thing is defining the public API of the library. The export mechanism only provides the means to implement that higher-level goal. Consequently the macro from the example above should better be called something along the lines of ``FOO_API`` or ``FOO_PUBLIC_API``. It’s a clearer statement of the author’s actual intent.

Additionally the wrong level of abstraction leads to some situations where the macro names are weird or misleading. This happens on Windows when using a library as a dependency, for instance *foo* from above. In that situation the symbols are *imported* into the software being built, and the macro should really be called ``FOO_IMPORT``. Something similar happens when building a static library. Since exporting symbols is not a thing for static libraries the macro names don’t make sense at all. Raising the level of abstraction and naming the macros accordingly gets rid of these problems.

Sadly, some of the macro names in the header file created by *GenerateExportHeader* are hard-coded to the *export* naming scheme, which made the reimplementation necessary.


prac_configure_library_api()
----------------------------
Configures the functionality for defining the API of a library. This includes:

* Setting the default symbol visibility to *hidden* and hiding inlines (for shared libraries).
* Setting up related private and public compile definitions for the target.
* Generating a header with the usual API definition/symbol visibility macros.

This function may only be called for library targets. ::

    prac_configure_library_api(
        <lib_target>
        [HEADER_FILE <filepath>]
        [PREFIX <name>]
        [PRAGMA_ONCE | INCLUDE_GUARD_NAME <name>]
        [STATIC_LIB_DEFINE <name>]
        [BUILDING_LIB_DEFINE <name>]
        [API_DEFINE <name>]
        [INTERNAL_DEFINE <name>]
        [OMIT_DEPRECATION_DEFINES | [
          [DEPRECATED_DEFINE <name>]
          [DEPRECATED_MSG_DEFINE <name>]
          [DEPRECATED_API_DEFINE <name>]
          [DEPRECATED_API_MSG_DEFINE <name>]
          [DEPRECATED_INTERNAL_DEFINE <name>]
          [DEPRECATED_INTERNAL_MSG_DEFINE <name>]
        ]
        [PREAMBLE <content>]
        [APPENDIX <content>]
    )

All the default macro names are prefixed with an upper case version of ``PREFIX``.

Parameters:

``<lib_target>``
    Name of an existing library target.
``HEADER_FILE <filepath>``
    Path to the generated header file. A relative path is interpreted as relative to ``CMAKE_CURRENT_BINARY_DIR``. Defaults to ``${CMAKE_CURRENT_BINARY_DIR}/<prefix>_api.h``
``PREFIX <name>``
    Prefix used for macro names and for the header file name. Defaults to ``<lib_target>``.
``PRAGMA_ONCE`` and ``INCLUDE_GUARD_NAME <name>``
    Configures the include guard style used by the header file. Defaults to an include guard macro with a name derived from ``PREFIX``.
``STATIC_LIB_DEFINE <name>``
    The macro name for the flag indicating a static library. Defaults to ``<PREFIX>_STATIC_LIB``. If the target is a static library this define is added to its public compile definitions.
``BUILDING_LIB_DEFINE <name>``
    The macro name indicating that the library is built, not used. Defaults to ``<PREFIX>_BUILDING_LIB``. The target’s ``DEFINE_SYMBOL`` property is set to this value. Also used in the header file to detect how the header is used.
``API_DEFINE <name>``
    The macro name for marking symbols as part of the library’s public API. Such symbols are exported from shared libraries.
``INTERNAL_DEFINE <name>``
    The macro name for explicitly marking symbols as an internal implementation detail. Such symbols are not exported from shared libraries if the compiler supports that feature. Generally GCC and Clang do, MSVC does not.
``OMIT_DEPRECATION_DEFINES``
    If specified the deprecation macros are omitted from the generated header. All the other ``DEPRECATED_`` arguments are ignored.
``DEPRECATED_DEFINE <name>``, ``DEPRECATED_MSG_DEFINE <name>``, etc.
    The macro names for marking symbols as deprecated. Default names are based on the argument names. With the ``MSG`` variants it’s possible to provide an explaning message for each deprecated symbol. Also, convenience macros are available for marking something as a deprecated public/internal API symbol.
``PREAMBLE <content>``
    Inserted verbatim at the very beginning of the generated header, i.e. *before* the include guard. Intended for adding a start-of-file comment.
``APPENDIX <content>``
    Inserted verbatim at the end of the generated header *before* closing the include guard. Any custom code you want to have in the header should go here.

Output variable:

``<lib_target>_API_HEADER_FILE``
    Set to the full absolute path of the generated header file.


Versioning.cmake
================

Provides utility functions for versioning including semantic versioning support.


prac_project_semver()
---------------------
Sets a semantic version number for the current project. Intended as a replacement for
``project(VERSION)`` which lacks semver support. Accordingly creates very similar output
variables in the caller’s scope. ::

    prac_project_semver(<semver>)

The function takes a single parameter, a valid semantic version number according to the `SemVer 2.0.0 specification <https://semver.org/>`_. The version follows this pattern::

    major.minor.patch-pre+meta

The first three components (the “version triple”) must be present. Each component of the triple must be a non-negative integer without leading zeros. The pre and meta components are optional.

Output variables:

``<project_name>_VERSION_MAJOR`` and ``PROJECT_VERSION_MAJOR``
    Contains the major version number.
``<project_name>_VERSION_MINOR`` and ``PROJECT_VERSION_MINOR``
    Contains the minor version number.
``<project_name>_VERSION_PATCH`` and ``PROJECT_VERSION_PATCH``
    Contains the patch version number.
``<project_name>_VERSION_PRE`` and ``PROJECT_VERSION_PRE``
    Contains the pre-release info without the leading dash. Empty if not present.
``<project_name>_VERSION_META`` and ``PROJECT_VERSION_META``
    Contains the version metadata without the leading plus sign. Empty if not present.
``<project_name>_VERSION`` and ``PROJECT_VERSION``
    Contains the dot-separated major, minor and patch version (e.g. *1.0.0*). Does not contain pre-release or metadata info to remain compatible with the corresponding all-integer variable created by the ``project()`` command.
``<project_name>_VERSION_FULL`` and ``PROJECT_VERSION_FULL``
    Version up to and including the pre-release component, but without the metadata.
``<project_name>_VERSION_FULL_META`` and ``PROJECT_VERSION_FULL_META``
    Complete version number including the pre-release and metadata components.
``CMAKE_PROJECT_VERSION``
    If ``prac_project_semver()`` is called from the top-level CMakeLists.txt, set to the same value as ``PROJECT_VERSION``. Mimics the built-in ``project()`` command’s behaviour introduced in CMake 3.12.

Some of the version variables might contain the same string. For example, if no pre-release component is present ``PROJECT_VERSION`` and ``PROJECT_VERSION_FULL`` are identical.

Semantic versioning has no equivalent of the numeric ``TWEAK`` version component supported by the ``project()`` command. ``prac_project_semver()`` does not try to mimic that component.

The pre-release and metadata components are not fully validated because they have no meaning for version compatibility checks. If you want to validate further yourself you may assume that both components only contain valid characters.


prac_write_package_version_file()
---------------------------------
Generates a *<package>-config-version.cmake* file for an installed package.

This function is the same as ``write_basic_package_version_file()`` with added semantic versioning support. Its signature is almost the same::

    prac_write_package_version_file(
        <output_file>
        [COMPATIBILITY {SemanticVersion|<built-in_specifier>}]
        [VERSION {<semantic_version>|<major.minor.patch>}]
        [ARCH_INDEPENDENT]
    )

If ``COMPATIBILITY`` is not ``SemanticVersion`` all arguments are simply forwarded to ``write_basic_package_version_file()``. As a result it depends on the CMake version what the list of ``<built-in_specifier>`` is and whether ``ARCH_INDEPENDENT`` is supported. ``COMPATIBILITY`` defaults to ``SemanticVersion``.

For ``SemanticVersion`` mode the following applies regardless of the CMake version:

``<output_file>``
    Path to the output file. Relative paths are interpreted as relative to ``CMAKE_CURRENT_BINARY_DIR``.
``VERSION``
    A valid semantic version string. The version of the installed package is reported exactly as specified with this argument. However, as per semver rules, only major and minor version are relevant for the compatibility check. Defaults to the current project’s version. Project versions set with both ``package(VERSION)`` or ``prac_project_semver()`` are supported.
``ARCH_INDEPENDENT``
    Works similar to the built-in variant. It is intended for header-only libraries or other packages without binaries. If set the architecture of the installed package is not checked. Otherwise the architectures must match exactly. For example, an installed package built for Linux x86_64 can only be used when building for Linux x86_64 as well.


prac_write_version_header()
---------------------------
Creates a header file with version information usable from both C++ and C. ::

    prac_write_version_header(
        <output_file>
        [VERSION {<semver>|<major.minor.patch.tweak>}]
        [MACRO_PREFIX <name>]
        [PRAGMA_ONCE | INCLUDE_GUARD_NAME <name>]
        [PREAMBLE <content>]
        [APPENDIX <content>]
    )

Parameters:

``<output_file>``
    Path to the output file. Relative paths are interpreted as relative to ``CMAKE_CURRENT_BINARY_DIR``.
``VERSION``
    Either a semantic version string or a numeric version with 1–4 components. Defaults to the current project’s version as set by either ``project(VERSION)`` or ``prac_project_semver()``.
``MACRO_PREFIX``
    Prefix for macro names. Automatically converted to upper case. Defaults to to the ``PROJECT_NAME``.
``PRAGMA_ONCE`` and ``INCLUDE_GUARD_NAME <name>``
    Configures the include guard style used by the header file. Defaults to an include guard macro with a name derived from ``MACRO_PREFIX``.
``PREAMBLE <content>``
    Inserted verbatim at the very beginning of the generated header, i.e. *before* the include guard. Intended for adding a start-of-file comment.
``APPENDIX <content>``
    Inserted verbatim at the end of the generated header *before* closing the include guard. Any custom code you want to have in the header should go here.


prac_parse_semantic_version()
-----------------------------
Parses a semantic version string. ::

    prac_parse_semantic_version(<semver_string>)

The function takes a single parameter, a valid semantic version number according to the `SemVer 2.0.0 specification <https://semver.org/>`_. The version follows this pattern::

    major.minor.patch-pre+meta

The first three components (the “version triple”) must be present. Each component of the triple must be a non-negative integer without leading zeros. The pre and meta components are optional.

Outputs:

``PRAC_VER_ERROR``
    Empty if parsing succeeds. Otherwise contains an error message. In the error case the state of the other output variables is undefined. Do not rely on them.
``PRAC_VER_MAJOR``, ``PRAC_VER_MINOR``, ``PRAC_VER_PATCH``
    The major, minor and patch version numbers.
``PRAC_VER_TRIPLE``
    The major, minor and patch version combined into an *x.y.z* version triple string.
``PRAC_VER_PRE``, ``PRAC_VER_PRE_DASH``
    The pre-release component excluding and including the leading dash. Both variables are empty if the version has no pre-release component.
``PRAC_VER_META``, ``PRAC_VER_META_PLUS``
    The metadata component excluding and including the leading plus sign. Both variables are empty if the version has no metadata component.


prac_parse_numeric_version()
----------------------------
Parses a numeric version number with up to four components: *major.minor.patch.tweak*. ::

    prac_parse_numeric_version(<version>)

Each component must be a non-negative integer without leading zeros. The major version must be present, the other three are optional.

Outputs:

``PRAC_VER_ERROR``
    Empty if parsing succeeds. Otherwise contains an error message. In the error case the state of the other output variables is undefined. Do not rely on them.
``PRAC_VER_MAJOR``, ``PRAC_VER_MINOR``, ``PRAC_VER_PATCH``, ``PRAC_VER_TWEAK``
    The four components of the version. Missing components are empty.
``PRAC_VER_LEN``
    The number of components contained in the version (1–4).


prac_parse_any_version()
------------------------
Parses any kind of supported version string. ::

    prac_parse_any_version(<semver_or_numeric_version>)

First tries to parse the string as a semantic version. If that fails tries parsing as a numeric version.

Outputs:

``PRAC_VER_ERROR``
    Empty if parsing succeeds. Otherwise contains the combined error messages from both parsing attempts. In the error case the state of the other output variables is undefined. Do not rely on them.
``PRAC_VER_IS_SEMANTIC``
    `TRUE` if the version was successfully parsed as a semantic version, `FALSE` otherwise. Semantic parsing is attempted first. A version valid in both schemes will be detected as semantic.
*parsing result*
    Several output variables depending on which parsing attempt succeeded. See ``prac_parse_semantic_version()`` and ``prac_parse_numeric_version()`` for details.

.. @@generated-cmake-end@@


.. _C++ Library API:

C++ Library Api
###############

.. @@generated-cpp@@

assert.hpp
==========

Provides C++ assertions in several forms.

``PRAC_ASSERT(boolean_expression);``
    The simplest assert. If *boolean_expression* evaluates to ``true`` it does nothing. Otherwise it prints information about the assertion to *stderr*, then calls ``std::abort()``. The error information includes the expression itself and the location of the ``PRAC_ASSERT()`` call: file, line and function.
``PRAC_ASSERT_MSG(boolean_expression, message);``
    Works like the simple assert above, but in the error case also prints the given *message* in addition to the normal error info.
``PRAC_ASSERT_FMT(boolean_expression, format_string, args...);``
    Works like the simple assert above, but in the error case also prints the given formatted message in addition to the normal error info. Requires the `fmt library`_ and is only available if ``PRAC_FMTLIB`` is defined. *format_string* and *args...* are passed to ``fmt::format()``.

By default the assertions are conditionally enabled like the standard library’s ``assert()``. They are active when the ``NDEBUG`` macro is not defined, otherwise the assertion calls do nothing. But unlike the standard assert redefinition of ``NDEBUG`` and multi-include is not supported. Instead *assert.hpp* is a normal guarded header file included at most once in any given translation unit.

After including *assert.hpp* the ``PRAC_ASSERTS_ACTIVE`` macro is defined without a value if assertions are active. Otherwise the macro is not defined.

.. _assert-config:

Assertions can be configured through `Import-time Configuration`_. The options specific to the *assert.hpp* header are:

``PRAC_ASSERTS_ALWAYS_ON`` and ``PRAC_ASSERTS_ALWAYS_OFF``
    If true enables (``_ON``) or disables (``_OFF``) assertions regardless of how ``NDEBUG`` is set. Both variables cannot be true at the same time.
``PRAC_FMTLIB`` and ``PRAC_FMTLIB_HEADERONLY``
    If true enables the ``PRAC_ASSERT_FMT`` macros with support for a formatted message.

    The `fmt library`_ is used for formatting. You are responsible for making *fmt* available. In CMake call ``find_package(fmt)`` before calling ``find_package(prac)`` or ``add_subdirectory(prac)``.

    In CMake ``PRAC_FMTLIB`` uses the compiled version of *fmt*, ``PRAC_FMTLIB_HEADERONLY`` uses the header-only version. Both variables cannot be true at the same time. If you set the preprocessor defines directly both are treated the same. Setting either one is sufficient.

The assertion macros and the ``PRAC_ASSERTS_ACTIVE`` flag respect the ``PRAC_UNPREFIXED_MACROS`` option.

.. _fmt library: https://fmt.dev/


scope_guards.hpp
================

Provides RAII based scope guards for executing code when leaving a scope. Modelled after Andrei Alexandrescu’s CppCon 2015 talk “Declarative Control Flow”.

Provides three macros:

``PRAC_SCOPE_EXIT``
    Executes code unconditionally on scope exit.
``PRAC_SCOPE_SUCCESS``
    Executes code only when no exception is thrown in the scope.
``PRAC_SCOPE_FAIL``
    Executes code only when an exception is thrown in the scope.

The macros must be followed by a block containing the code to execute. For example::

    PRAC_SCOPE_EXIT {
        do_some_cleanup();
    };

This block is the body of a lambda. Note the mandatory semicolon at its end. The lambda catches everything by reference and takes no arguments. Exceptions may only be thrown from the lamdba body for ``PRAC_SCOPE_SUCCESS``.

The macro respect the ``PRAC_UNPREFIXED_MACROS`` `Import-time Configuration`_.


sysinfo.h
=========

A pragmatic C++ and C header for compile-time detection of the target platform and some target platform features.

It is designed to be minimal. It only uses the preprocessor, has no further dependencies and does not include any other headers. Also it does not try to handle every obscure platform you can think of. Instead it focuses on the major platforms (Linux, BSD, Android, Apple, Windows) on usual hardware (x86, ARM), either 64bit or 32bit.

All macros are either defined without a value or not defined at all.

Main macros to identify the target OS/platform. Only one of these macros is defined at a given time:

==========================  =======================================
macro                       defined when compiling for
==========================  =======================================
``PRAC_SYSINFO_ANDROID``    Android
``PRAC_SYSINFO_APPLE``      any Darwin-based Apple OS
``PRAC_SYSINFO_BSD``        FreeBSD, NetBSD, OpenBSD, DragonFly BSD
``PRAC_SYSINFO_CYGWIN``     Cygwin
``PRAC_SYSINFO_LINUX``      Linux
``PRAC_SYSINFO_WINDOWS``    Microsoft Windows
==========================  =======================================

Additional platform info, defined in addition to the main macros above:

==========================  =====================================================
macro                       defined when compiling for
==========================  =====================================================
``PRAC_SYSINFO_MINGW``      MinGW or MinGW-w64, in addition to ``WINDOWS``
``PRAC_SYSINFO_UNIX``       a Unix-like system, applies to all except ``WINDOWS``
==========================  =====================================================

Macros to identify platform details:

===================================  =======================================================
macro                                defined if
===================================  =======================================================
``PRAC_SYSINFO_64BIT``               target platform is 64bit
``PRAC_SYSINFO_32BIT``               target platform is 32bit
``PRAC_SYSINFO_LITTLE_ENDIAN``       target platform has little endian byte order [1]
``PRAC_SYSINFO_BIG_ENDIAN``          target platform has big endian (network) byte order [1]
``PRAC_SYSINFO_RTTI``                C++ RTTI is enabled
``PRAC_SYSINFO_EXCEPTIONS``          C++ exceptions are enabled
===================================  =======================================================

+ [1] MSVC does not provide a preprocessor way to detect the endianness. Little endian is assumed because Windows only runs on little endian platforms. This does not affect Clang in MSVC ABI mode because it provides its normal endianness detection even in that mode.

