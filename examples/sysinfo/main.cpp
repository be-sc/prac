// clang-format off
#include "../../lib_cpp/include/prac/sysinfo.h"
#include <iostream>
#include <string>

int main()
{
    std::cout << "PRAC_SYSINFO_ANDROID: " <<
#ifdef PRAC_SYSINFO_ANDROID
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_LINUX: " <<
#ifdef PRAC_SYSINFO_LINUX
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_APPLE: " <<
#ifdef PRAC_SYSINFO_APPLE
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_BSD: " <<
#ifdef PRAC_SYSINFO_BSD
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_WINDOWS: " <<
#ifdef PRAC_SYSINFO_WINDOWS
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_MINGW: " <<
#ifdef PRAC_SYSINFO_MINGW
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_CYGWIN: " <<
#ifdef PRAC_SYSINFO_CYGWIN
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_UNIX: " <<
#ifdef PRAC_SYSINFO_UNIX
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_64BIT: " <<
#ifdef PRAC_SYSINFO_64BIT
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_32BIT: " <<
#ifdef PRAC_SYSINFO_32BIT
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_LITTLE_ENDIAN: " <<
#ifdef PRAC_SYSINFO_LITTLE_ENDIAN
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_BIG_ENDIAN: " <<
#ifdef PRAC_SYSINFO_BIG_ENDIAN
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_RTTI: " <<
#ifdef PRAC_SYSINFO_RTTI
    "yes\n";
#else
    "no\n";
#endif

    std::cout << "PRAC_SYSINFO_EXCEPTIONS: " <<
#ifdef PRAC_SYSINFO_EXCEPTIONS
    "yes\n";
#else
    "no\n";
#endif
}

// clang-format on
