#!/usr/bin/env python3

import argparse
import re
import shutil
import subprocess
import sys
from pathlib import Path
from typing import Sequence

HERE_DIR = Path(__file__).parent
PRAC_IMPL_DIR = Path(HERE_DIR.resolve(), '../../lib_cmake').resolve()


def clean_build_dir(build_dir):
    if build_dir.exists():
        shutil.rmtree(build_dir)
    build_dir.mkdir(parents=True)


def main() -> int:
    parser = argparse.ArgumentParser()
    # yapf: disable
    parser.add_argument(
        'testsuites',
        nargs='*',
        help='A list of existing test suite directory names.'
    )
    parser.add_argument(
        '--keep',
        '-k',
        action='store_true',
        help='Keep build directories of successful test suites.'
    )
    parser.add_argument(
        '--cmake',
        '-c',
        default='cmake',
        help='Command for running CMake. Defaults to: cmake'
    )
    # yapf: enable

    args = parser.parse_args()
    suites = args.testsuites
    if not suites:
        suites = [
            dir.name
            for dir in HERE_DIR.iterdir()
            if dir.is_dir() and not dir.name.startswith('build')
        ]

    suites.sort()
    succeeded = run_testsuites(suites, keep_successful_build_dirs=args.keep, cmake_cmd=args.cmake)
    return 0 if succeeded else 1


def run_testsuites(
    suites: Sequence[str], *, keep_successful_build_dirs: bool, cmake_cmd: str
) -> bool:
    suite_count: int = len(suites)
    successful_count: int = suite_count
    cmake_min_version = extract_cmake_min_version_from_cmakelists()

    for suite_name in suites:
        print('\n')
        print('=' * 70)
        print(f'Test suite: {suite_name}')
        print('=' * 70)

        suite_dir = HERE_DIR / suite_name
        if not suite_dir.exists():
            print('Test suite directory does not exist.', file=sys.stderr)
            successful_count -= 1
            continue

        sys.stdout.flush()
        sys.stderr.flush()

        build_dir = HERE_DIR / f'build_{suite_name}'
        clean_build_dir(build_dir)
        status = subprocess.run([
            cmake_cmd,
            f'-DPRAC_CM_MINVER={cmake_min_version}',
            f'-DCMAKE_MODULE_PATH={PRAC_IMPL_DIR}',
            '-S',
            str(suite_dir),
            '-B',
            str(build_dir)
        ])

        if status.returncode == 0:
            print(f'-- Test suite `{suite_name}` ran successfully.')
        else:
            successful_count -= 1

        if successful_count and (not keep_successful_build_dirs) and build_dir.exists():
            shutil.rmtree(build_dir)

    print(
        f'\nRan {suite_count} test suites. '
        f'{successful_count} succeeded, {suite_count-successful_count} failed.'
    )

    return successful_count == suite_count


def extract_cmake_min_version_from_cmakelists() -> str:
    # regex to find min required CMake version in CMakeLists.txt. Line looks like this:
    # cmake_minimum_required(VERSION x.y FATAL_ERROR)
    CMAKE_MIN_VERSION_RE = re.compile(
        rf'^cmake_minimum_required\(VERSION ([0-9]+(?:.[0-9]+)+) FATAL_ERROR\)$')

    with Path(HERE_DIR, '../../CMakeLists.txt').open('r') as fp:
        content = fp.readlines()

    for line in content:
        match = re.match(CMAKE_MIN_VERSION_RE, line)
        if match is not None:
            return match[1]

    raise RuntimeError('CMake min version not found in CMakeLists.txt.')


if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        print('Aborted manually.', file=sys.stderr)
        sys.exit(1)
