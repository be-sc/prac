macro(reset_version_variables)
    set(PRAC_VER_ERROR "<erroneous value>")
    set(PRAC_VER_MAJOR "<erroneous value>")
    set(PRAC_VER_MINOR "<erroneous value>")
    set(PRAC_VER_PATCH "<erroneous value>")
    set(PRAC_VER_TRIPLE "<erroneous value>")
    set(PRAC_VER_PRE "<erroneous value>")
    set(PRAC_VER_PRE_DASH "<erroneous value>")
    set(PRAC_VER_META "<erroneous value>")
    set(PRAC_VER_META_PLUS "<erroneous value>")
    set(PRAC_VER_TWEAK "<erroneous value>")
    set(PRAC_VER_IS_SEMANTIC "<erroneous value>")
endmacro()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

# numeric versions

macro(test_valid_numeric_version version len major minor patch tweak  line)
    start_test("parsing valid numeric version `${version}` succeeds")
        reset_version_variables()
        prac_parse_numeric_version("${version}")

        check_equal(PRAC_VER_ERROR "" ${line})

        if (NOT practest_last_assertion_failed)
            check_equal(PRAC_VER_LEN "${len}" ${line})
            check_equal(PRAC_VER_MAJOR "${major}" ${line})
            check_equal(PRAC_VER_MINOR "${minor}" ${line})
            check_equal(PRAC_VER_PATCH "${patch}" ${line})
            check_equal(PRAC_VER_TWEAK "${tweak}" ${line})
        endif()
    end_test()
endmacro()

macro(test_invalid_numeric_version version line)
    start_test("parsing invalid numeric version `${version}` fails")
        reset_version_variables()
        prac_parse_numeric_version("${version}")

        # Parsing succeeded but shouldn’t have.
        check_not_equal(PRAC_VER_ERROR "" ${line})

        # Error variable was not set at all.
        check_not_equal(PRAC_VER_ERROR "<erroneous value>" ${line})
    end_test()
endmacro()

# semantic versions

macro(test_valid_semantic_version version major minor patch pre meta line)
    start_test("parsing valid semantic version `${version}` succeeds")
        reset_version_variables()
        prac_parse_semantic_version("${version}")

        check_equal(PRAC_VER_ERROR "" ${line})

        if (NOT practest_last_assertion_failed)
            check_equal(PRAC_VER_MAJOR "${major}" ${line})
            check_equal(PRAC_VER_MINOR "${minor}" ${line})
            check_equal(PRAC_VER_PATCH "${patch}" ${line})
            check_equal(PRAC_VER_TRIPLE "${PRAC_VER_MAJOR}.${PRAC_VER_MINOR}.${PRAC_VER_PATCH}" ${line})
            check_equal(PRAC_VER_PRE "${pre}" ${line})
            check_equal(PRAC_VER_META "${meta}" ${line})

            if ("${pre}" STREQUAL "")
                check_equal(PRAC_VER_PRE_DASH "" ${line})
            else()
                check_equal(PRAC_VER_PRE_DASH "-${pre}" ${line})
            endif()

            if ("${meta}" STREQUAL "")
                check_equal(PRAC_VER_META_PLUS "" ${line})
            else()
                check_equal(PRAC_VER_META_PLUS "+${meta}" ${line})
            endif()
        endif()
    end_test()
endmacro()

macro(test_invalid_semantic_version version line)
    start_test("parsing invalid semantic version `${version}` fails")
        reset_version_variables()
        prac_parse_semantic_version("${version}")

        # Parsing succeeded but shouldn’t have.
        check_not_equal(PRAC_VER_ERROR "" ${line})

        # Error variable was not set at all.
        check_not_equal(PRAC_VER_ERROR "<erroneous value>" ${line})
    end_test()
endmacro()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# tests

test_valid_numeric_version("1"          1   "1"  ""   ""   ""   ${CMAKE_CURRENT_LIST_LINE})
test_valid_numeric_version("1.2"        2   "1"  "2"  ""   ""   ${CMAKE_CURRENT_LIST_LINE})
test_valid_numeric_version("1.2.3"      3   "1"  "2"  "3"  ""   ${CMAKE_CURRENT_LIST_LINE})
test_valid_numeric_version("1.2.3.4"    4   "1"  "2"  "3"  "4"  ${CMAKE_CURRENT_LIST_LINE})
test_valid_numeric_version("0.20.34"    3   "0"  "20" "34" ""   ${CMAKE_CURRENT_LIST_LINE})
test_valid_numeric_version("0.0.0.0"    4   "0"  "0"  "0"  "0"  ${CMAKE_CURRENT_LIST_LINE})

test_invalid_numeric_version(""             ${CMAKE_CURRENT_LIST_LINE})  # empty
test_invalid_numeric_version("1.2.3.4.5"    ${CMAKE_CURRENT_LIST_LINE})  # too many components
test_invalid_numeric_version("01.2.3"       ${CMAKE_CURRENT_LIST_LINE})  # leading zero
test_invalid_numeric_version("1.02.3"       ${CMAKE_CURRENT_LIST_LINE})  # leading zero
test_invalid_numeric_version("1.2.03"       ${CMAKE_CURRENT_LIST_LINE})  # leading zero
test_invalid_numeric_version("1.2.3.04"     ${CMAKE_CURRENT_LIST_LINE})  # leading zero
test_invalid_numeric_version("a.2.3.4"      ${CMAKE_CURRENT_LIST_LINE})  # non-numeric component
test_invalid_numeric_version("1.b.3.4"      ${CMAKE_CURRENT_LIST_LINE})  # non-numeric component
test_invalid_numeric_version("1.2.c.4"      ${CMAKE_CURRENT_LIST_LINE})  # non-numeric component
test_invalid_numeric_version("1.2.3.d"      ${CMAKE_CURRENT_LIST_LINE})  # non-numeric component
test_invalid_numeric_version("no version"   ${CMAKE_CURRENT_LIST_LINE})  # some nonsense string


test_valid_semantic_version("1.2.3"                             "1"  "2"  "3"  ""  ""                           ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("0.20.34"                           "0"  "20" "34" ""  ""                           ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("0.0.0"                             "0"  "0"  "0"  ""  ""                           ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("1.2.3-beta"                        "1"  "2"  "3"  "beta"  ""                       ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("1.2.3-beta.5-2"                    "1"  "2"  "3"  "beta.5-2"   ""                  ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("1.2.3+2019-01-01.12-00"            "1"  "2"  "3"  ""           "2019-01-01.12-00"  ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("1.2.3-beta.5-2+2019-01-01.12-00"   "1"  "2"  "3"  "beta.5-2"   "2019-01-01.12-00"  ${CMAKE_CURRENT_LIST_LINE})
test_valid_semantic_version("1.2.3-0+0"                         "1"  "2"  "3"  "0"  "0"                         ${CMAKE_CURRENT_LIST_LINE})

test_invalid_semantic_version(""                ${CMAKE_CURRENT_LIST_LINE})  # empty
test_invalid_semantic_version("1"               ${CMAKE_CURRENT_LIST_LINE})  # minor+patch version missing
test_invalid_semantic_version("1.0"             ${CMAKE_CURRENT_LIST_LINE})  # patch version missing
test_invalid_semantic_version("1.2.3.4"         ${CMAKE_CURRENT_LIST_LINE})  # version quadruple
test_invalid_semantic_version("01.2.3"          ${CMAKE_CURRENT_LIST_LINE})  # leading major zero
test_invalid_semantic_version("1.02.3"          ${CMAKE_CURRENT_LIST_LINE})  # leading minor zero
test_invalid_semantic_version("1.2.03"          ${CMAKE_CURRENT_LIST_LINE})  # leading patch zero
test_invalid_semantic_version("a.2.3"           ${CMAKE_CURRENT_LIST_LINE})  # non-numeric major version
test_invalid_semantic_version("1.b.3"           ${CMAKE_CURRENT_LIST_LINE})  # non-numeric minor version
test_invalid_semantic_version("1.2.c"           ${CMAKE_CURRENT_LIST_LINE})  # non-numeric patch version
test_invalid_semantic_version("1.0-pre"         ${CMAKE_CURRENT_LIST_LINE})  # patch missing when pre-release present
test_invalid_semantic_version("1+meta"          ${CMAKE_CURRENT_LIST_LINE})  # minor+patch missing when metadata present
test_invalid_semantic_version("1.0.0_beta"      ${CMAKE_CURRENT_LIST_LINE})  # underscore instead of dash
test_invalid_semantic_version("1.0.0-beta,2"    ${CMAKE_CURRENT_LIST_LINE})  # illegal character in pre-release
test_invalid_semantic_version("1.0.0+me,ta"     ${CMAKE_CURRENT_LIST_LINE})  # illegal character in metadata


start_test("universal version detected as semantic")
    reset_version_variables()
    prac_parse_any_version("1.2.3")
    check_equal(PRAC_VER_ERROR "" ${CMAKE_CURRENT_LIST_LINE})
    check_true(PRAC_VER_IS_SEMANTIC ${CMAKE_CURRENT_LIST_LINE})
end_test()

start_test("semantic version detected as such")
    reset_version_variables()
    prac_parse_any_version("1.2.3-alpha.1")
    check_equal(PRAC_VER_ERROR "" ${CMAKE_CURRENT_LIST_LINE})
    check_true(PRAC_VER_IS_SEMANTIC ${CMAKE_CURRENT_LIST_LINE})
end_test()

start_test("numeric version detected as such")
    reset_version_variables()
    prac_parse_any_version("1.2.3.4")
    check_equal(PRAC_VER_ERROR "" ${CMAKE_CURRENT_LIST_LINE})
    check_false(PRAC_VER_IS_SEMANTIC ${CMAKE_CURRENT_LIST_LINE})
end_test()

start_test("invalid version detected as such")
    reset_version_variables()
    prac_parse_any_version("1.2.3.4.5")
    check_not_equal(PRAC_VER_ERROR "" ${CMAKE_CURRENT_LIST_LINE})
    check_not_equal(PRAC_VER_ERROR "<erroneous value>" ${CMAKE_CURRENT_LIST_LINE})
end_test()
