// Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
/**
Provides C++ assertions in several forms.

``PRAC_ASSERT(boolean_expression);``
    The simplest assert. If *boolean_expression* evaluates to ``true`` it does nothing. Otherwise it prints information about the assertion to *stderr*, then calls ``std::abort()``. The error information includes the expression itself and the location of the ``PRAC_ASSERT()`` call: file, line and function.
``PRAC_ASSERT_MSG(boolean_expression, message);``
    Works like the simple assert above, but in the error case also prints the given *message* in addition to the normal error info.
``PRAC_ASSERT_FMT(boolean_expression, format_string, args...);``
    Works like the simple assert above, but in the error case also prints the given formatted message in addition to the normal error info. Requires the `fmt library`_ and is only available if ``PRAC_FMTLIB`` is defined. *format_string* and *args...* are passed to ``fmt::format()``.

By default the assertions are conditionally enabled like the standard library’s ``assert()``. They are active when the ``NDEBUG`` macro is not defined, otherwise the assertion calls do nothing. But unlike the standard assert redefinition of ``NDEBUG`` and multi-include is not supported. Instead *assert.hpp* is a normal guarded header file included at most once in any given translation unit.

After including *assert.hpp* the ``PRAC_ASSERTS_ACTIVE`` macro is defined without a value if assertions are active. Otherwise the macro is not defined.

.. _assert-config:

Assertions can be configured through `Import-time Configuration`_. The options specific to the *assert.hpp* header are:

``PRAC_ASSERTS_ALWAYS_ON`` and ``PRAC_ASSERTS_ALWAYS_OFF``
    If true enables (``_ON``) or disables (``_OFF``) assertions regardless of how ``NDEBUG`` is set. Both variables cannot be true at the same time.
``PRAC_FMTLIB`` and ``PRAC_FMTLIB_HEADERONLY``
    If true enables the ``PRAC_ASSERT_FMT`` macros with support for a formatted message.

    The `fmt library`_ is used for formatting. You are responsible for making *fmt* available. In CMake call ``find_package(fmt)`` before calling ``find_package(prac)`` or ``add_subdirectory(prac)``.

    In CMake ``PRAC_FMTLIB`` uses the compiled version of *fmt*, ``PRAC_FMTLIB_HEADERONLY`` uses the header-only version. Both variables cannot be true at the same time. If you set the preprocessor defines directly both are treated the same. Setting either one is sufficient.

The assertion macros and the ``PRAC_ASSERTS_ACTIVE`` flag respect the ``PRAC_UNPREFIXED_MACROS`` option.

.. _fmt library: https://fmt.dev/
*/
// Because of the macro heavy code clang-format is not useful here.
// clang-format off

#pragma once

#ifdef PRAC_FMTLIB_HEADERONLY
    // makes sure we can rely on PRAC_FMTLIB alone from here
    #define PRAC_FMTLIB
#endif

#ifdef PRAC_FMTLIB
    // Include even if asserts are disabled. Otherwise fmt calls outside the assert macros
    // could fail because of the missing include.
    #include <fmt/core.h>
#endif

#if defined(PRAC_ASSERTS_ALWAYS_ON) && defined(PRAC_ASSERTS_ALWAYS_OFF)
    #error "Assertions configured to be always on and always off at the same time."
#endif

#if defined(PRAC_ASSERTS_ALWAYS_ON) || (!defined(NDEBUG) && !defined(PRAC_ASSERTS_ALWAYS_OFF))
    #define PRAC_ASSERTS_ACTIVE

    #if defined(PRAC_UNPREFIXED_MACROS)
        #define ASSERTS_ACTIVE
    #endif
#endif


#if !defined(PRAC_ASSERTS_ACTIVE)
    #define PRAC_ASSERT(expr) static_cast<void>(false && (expr))
    #define PRAC_ASSERT_MSG(expr, msg) static_cast<void>(false && (expr))

    #ifdef PRAC_FMTLIB
        #define PRAC_ASSERT_FMT(expr, ...) static_cast<void>(false && (expr))
    #endif
#else
    namespace prac
    {
    namespace detail
    {
        [[noreturn]] extern void abort_after_failed_assert(
            const char* expr, const char* file, int line, const char* funcsig, const char* msg);

        [[noreturn]] extern void abort_after_failed_assert(
            const char* expr, const char* file, int line, const char* funcsig);
    }  // namespace internal
    }  // namespace prac

    #if defined(__GNUC__) || defined(__clang__)
        #define PRAC_DETAIL_FUNC_SIG __PRETTY_FUNCTION__
    #elif defined(_MSC_VER)
        #define PRAC_DETAIL_FUNC_SIG __FUNCSIG__
    #else
        #define PRAC_DETAIL_FUNC_SIG __func__
    #endif

    #define PRAC_ASSERT(expr)                         \
        static_cast<bool>(expr)                         \
        ? static_cast<void>(0)                          \
        : ::prac::detail::abort_after_failed_assert(  \
            #expr, __FILE__, __LINE__, PRAC_DETAIL_FUNC_SIG)

    #define PRAC_ASSERT_MSG(expr, msg)                \
        static_cast<bool>(expr)                         \
        ? static_cast<void>(0)                          \
        : ::prac::detail::abort_after_failed_assert(  \
            #expr, __FILE__, __LINE__, PRAC_DETAIL_FUNC_SIG, (msg))

    #ifdef PRAC_FMTLIB
        #define PRAC_ASSERT_FMT(expr, ...)            \
        static_cast<bool>(expr)                         \
        ? static_cast<void>(0)                          \
        : ::prac::detail::abort_after_failed_assert(  \
            #expr, __FILE__, __LINE__, PRAC_DETAIL_FUNC_SIG, fmt::format(__VA_ARGS__).c_str())
    #endif
#endif

#if defined(PRAC_UNPREFIXED_MACROS)
    #define ASSERT(expr) PRAC_ASSERT(expr)
    #define ASSERT_MSG(expr, msg) PRAC_ASSERT_MSG(expr, msg)

    #ifdef PRAC_FMTLIB
        #define ASSERT_FMT(expr, ...) PRAC_ASSERT_FMT(expr, __VA_ARGS__)
    #endif
#endif
