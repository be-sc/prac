# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# test case definition

macro(start_test name)
    set(practest_current_test "${name}")
    message(STATUS "Test: \"${name}\"")
endmacro()

macro(end_test)
    set(practest_current_test)
    set(practest_last_assertion_failed FALSE)
endmacro()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# test suite status

function(abort_if_testsuite_failed)
    get_property(failed_tests GLOBAL PROPERTY practest_failed_tests)
    list(REMOVE_DUPLICATES failed_tests)
    list(LENGTH failed_tests failed_count)

    if (${failed_count} GREATER 0)
        if (${failed_count} EQUAL 1)
            set(plural "")
        else()
            set(plural "s")
        endif()
        list(JOIN failed_tests "\"\n   \"" failed_tests_str)
        message(
            FATAL_ERROR
            " ${failed_count} test${plural} failed (see messages above for details)\n"
            "   \"${failed_tests_str}\""
        )
    endif()
endfunction()

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# test assertions

macro(fail_test msg line)
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line} "     ${msg}\n")
endmacro()

macro(check_equal actual_varname expected_value line)
    if (NOT "${${actual_varname}}" STREQUAL "${expected_value}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     Values not equal.\n"
            "     actual (${actual_varname}): ${${actual_varname}}\n"
            "     expected: ${expected_value}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()

macro(check_not_equal actual_varname expected_value line)
    if ("${${actual_varname}}" STREQUAL "${expected_value}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     Values should differ but do not.\n"
            "     actual (${actual_varname}): ${${actual_varname}}\n"
            "     expected: ${expected_value}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()


macro(check_true actual_varname line)
    if (NOT "${${actual_varname}}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     Value does not evaluate to true.\n"
            "     actual (${actual_varname}): ${${actual_varname}}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()


macro(check_false actual_varname line)
    if ("${${actual_varname}}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     Value does not evaluate to false.\n"
            "     actual (${actual_varname}): ${${actual_varname}}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()


macro(check_target_exists target_name line)
    if (NOT TARGET "${target_name}")
        set(practest_last_assertion_failed TRUE)
        pracprivate_print_test_failure(${line} "     Target `${target_name}` does not exist.\n")
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()

macro(check_target_prop target prop expected_value line)
    get_target_property(practest_propvalue ${target} ${prop})
    if ("${practest_propvalue}" STREQUAL "practest_propvalue-NOTFOUND")
        set(practest_propvalue "")
    endif()

    if (NOT "${practest_propvalue}" STREQUAL "${expected_value}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     Target `${target}`: property `${prop}` has wrong value.\n"
            "     actual: ${practest_propvalue}\n"
            "     expected: ${expected_value}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()


macro(check_list_contains listname value line)
    list(FIND ${listname} "${value}" index)
    if (index EQUAL -1)
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     In list named: ${listname}\n"
            "     Missing value: ${value}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()

macro(check_list_not_contains listname value line)
    list(FIND ${listname} "${value}" index)
    if (NOT index EQUAL -1)
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     In list named: ${listname}\n"
            "     Unexpected value: ${value}\n"
        )
    else()
        set(practest_last_assertion_failed FALSE)
    endif()
endmacro()


macro(check_files_equal actual_path expected_path line)
    if (NOT EXISTS "${expected_path}")
        message(FATAL_ERROR "Expected file does not exist: ${expected_path}")
    endif()

    if (NOT EXISTS "${actual_path}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     File equality: Actual file is missing: ${actual_path}\n"
        )
    else()
        file(READ "${actual_path}" pracprivate_actual_content)
        file(READ "${expected_path}" pracprivate_expected_content)

        if (NOT pracprivate_actual_content STREQUAL pracprivate_expected_content)
            set(practest_last_assertion_failed TRUE)
            set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
            pracprivate_print_test_failure(${line}
                "     Files do not have equal content.\n"
                "     Actual: ${actual_path}\n"
                "     Expected: ${expected_path}\n"
            )
        else()
            set(practest_last_assertion_failed FALSE)
        endif()

        unset(pracprivate_actual_content)
        unset(pracprivate_expected_content)
    endif()
endmacro()

macro(check_file_contains filepath needle line)
    if (NOT EXISTS "${filepath}")
        set(practest_last_assertion_failed TRUE)
        set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
        pracprivate_print_test_failure(${line}
            "     File is missing: ${filepath}\n"
        )
    else()
        file(READ "${filepath}" pracprivate_haystack)
        string(FIND "${pracprivate_haystack}" "${needle}" pracprivate_needle_pos)

        if (pracprivate_needle_pos EQUAL -1)
            set(practest_last_assertion_failed TRUE)
            set_property(GLOBAL APPEND PROPERTY practest_failed_tests "${practest_current_test}")
            pracprivate_print_test_failure(${line}
                "     Expected content missing from file: ${pracprivate_haystack}\n"
                "     Expected: ${needle}"
            )
        else()
            set(practest_last_assertion_failed FALSE)
        endif()
    endif()
endmacro()



# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# utils

function(practest_list_contains listname value outvarname)
    list(FIND ${listname} "${value}" index)
    if (index EQUAL -1)
        set(${outvarname} FALSE PARENT_SCOPE)
    else()
        set(${outvarname} TRUE PARENT_SCOPE)
    endif()
endfunction()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details


macro(pracprivate_print_test_failure line)
    message(
        NOTICE
        " >------------>\n"
        " ${CMAKE_CURRENT_LIST_FILE}:${line}: test failed: \"${practest_current_test}\"\n"
        ${ARGN}
        " <------------<"
    )
endmacro()