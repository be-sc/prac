# Prolog for Prolog_Epilog_Version
if (CMAKE_VERSION VERSION_LESS 3.20)
    set(${CMAKE_FIND_PACKAGE_NAME}_NOT_FOUND_MESSAGE
        "Prolog_Epilog_Version requires CMake 3.20 or later. "
        "You are running version ${CMAKE_VERSION}."
    )
    set(${CMAKE_FIND_PACKAGE_NAME}_FOUND FALSE)
    return()
endif()




include("${CMAKE_CURRENT_LIST_DIR}/prolog_epilog_version-targets.cmake")

# Epilog for prolog_epilog_version
