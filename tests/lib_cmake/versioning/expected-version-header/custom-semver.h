#ifndef TESTSUITE_VERSION_INFO
#define TESTSUITE_VERSION_INFO

#define TESTSUITE_VERSION_MAJOR 4
#define TESTSUITE_VERSION_MINOR 5
#define TESTSUITE_VERSION_PATCH 6
#define TESTSUITE_VERSION_PRE "pre"
#define TESTSUITE_VERSION_META "meta"

// major.minor.patch
#define TESTSUITE_VERSION "4.5.6"

// major.minor.patch-pre (-pre is optional)
#define TESTSUITE_VERSION_FULL "4.5.6-pre"

// major.minor.patch-pre+meta (-pre and +meta are optional)
#define TESTSUITE_VERSION_FULL_META "4.5.6-pre+meta"

#endif  // TESTSUITE_VERSION_INFO
