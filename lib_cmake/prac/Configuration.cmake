# Part of Prac by Bernd Schöler <besc@here-be-braces.com>. Public domain or CC0.
#[===[.rst:
Provides functions for project and target configuration.
]===]
include_guard(GLOBAL)


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Detects if the current *CMakeLists.txt* is the toplevel one or is used as a subproject. Equivalent to the ``PROJECT_IS_TOP_LEVEL`` variable introduced in CMake 3.21 but can be used before calling ``project()``. ::

    prac_detect_toplevel([<out_varname>])

If the ``<out_varname>`` is used it specifies the name of the output variable. If the function is called without any parameters the output variable is called ``IS_TOPLEVEL``.

Sets the output variable to ``TRUE`` if the current *CMakeLists.txt* is the top-level one, ``FALSE`` otherwise.
]]
function(prac_detect_toplevel)
    if (ARGC GREATER 0)
        set(out_varname ${ARGV0})
    else()
        set(out_varname IS_TOPLEVEL)
    endif()

    get_directory_property(parent_dir PARENT_DIRECTORY)

    if (parent_dir)
        set(${out_varname} FALSE PARENT_SCOPE)
    else()
        set(${out_varname} TRUE PARENT_SCOPE)
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Defines the namespace associated with the current project. Must be called after ``project()``. ::

    prac_project_namespace([<nsname>])

The namespace name may end in a double colon ``::`` which is removed from the output variables. It must not contain any other double colons. If ``<nsname>`` is empty or missing it defaults to the ``PROJECT_NAME``.

The final namespace name is stored in the variables ``PROJECT_NAMESPACE`` as well as ``<project-name>_NAMESPACE``.
]]
function(prac_project_namespace)
    if (NOT DEFINED PROJECT_NAME)
        message(FATAL_ERROR "This function must be called after project().")
    endif()

    if (${ARGC} EQUAL 0)
        # no arg at all: default to project name
        set(pracprivate_nsname "${PROJECT_NAME}")
    elseif ("${ARGV0}" STREQUAL "")
        # empty nsname: default to project name
        set(pracprivate_nsname "${PROJECT_NAME}")
    else()
        # nsname explicitly non-empty
        pracprivate_decolon_namesapce("${ARGV0}")
    endif()

    set(PROJECT_NAMESPACE "${pracprivate_nsname}" PARENT_SCOPE)
    set(${PROJECT_NAME}_NAMESPACE "${pracprivate_nsname}" PARENT_SCOPE)
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Adds a library including double colon namespace handling. ::

    prac_add_namespaced_library(<libname> [NAMESPACE <ns>] [<add_library_args...>])

If you intend to use Prac’s extensions to CMake’s install functionality, prefer this command to create library targets. It is designed to configure the new target so it works seamlessly with the extensions.

Parameters:

``<libname>``
    The unqualified name of the library.
``NAMESPACE <ns>``
    The namespace of the library. Can be given with or without a trailing double colon (``::``). Must not contain any other double colons. If empty or missing defaults to the ``<project-name>_NAMESPACE`` and, if that is not defined, to the ``PROJECT_NAME``.
``<add_library_args...>``
    Any arguments supported by the built-in ``add_library()`` command except the library name itself. These arguments are passed verbatim to ``add_library()``.

Output:

* Adds a library target ``<ns>_<libname>``.
* Adds an alias library target ``<ns>::<libname>``.
* Sets the library’s ``EXPORT_NAME`` property to ``<libname>``.
]]
macro(prac_add_namespaced_library libname)
    if (NOT DEFINED PROJECT_NAME)
        message(FATAL_ERROR "This command must be called after project().")
    endif()
    if ("${libname}" STREQUAL "")
        message(FATAL_ERROR "Library cannot be created with an empty name.")
    endif()

    pracprivate_parse_ns_args("${libname}" ${ARGN})

    add_library(${pracprivate_undername} ${pracprivate_passthrough_args})
    add_library(${pracprivate_colonname} ALIAS ${pracprivate_undername})
    set_target_properties(${pracprivate_undername} PROPERTIES EXPORT_NAME ${libname})

    unset(pracprivate_undername)
    unset(pracprivate_colonname)
    unset(pracprivate_passthrough_args)
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Adds an executable including double colon namespace handling. ::

    prac_add_namespaced_executable(<exename> [NAMESPACE <ns>] [<add_executable_args...>])

If you intend to use Prac’s extensions to CMake’s install functionality, prefer this command to create executable targets. It is designed to configure the new target so it works seamlessly with the extensions.

Parameters and output are equivalent to ``prac_add_namespaced_library()``.
]]
macro(prac_add_namespaced_executable exename)
    if (NOT DEFINED PROJECT_NAME)
        message(FATAL_ERROR "This command must be called after project().")
    endif()
    if ("${exename}" STREQUAL "")
        message(FATAL_ERROR "Executable cannot be created with an empty name.")
    endif()

    pracprivate_parse_ns_args("${exename}" ${ARGN})

    add_executable(${pracprivate_undername} ${pracprivate_passthrough_args})
    add_executable(${pracprivate_colonname} ALIAS ${pracprivate_undername})
    set_target_properties(${pracprivate_undername} PROPERTIES EXPORT_NAME ${exename})

    unset(pracprivate_undername)
    unset(pracprivate_colonname)
    unset(pracprivate_passthrough_args)
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Performs C++ and C compiler detection. This means:

* Detailed detection for the “big 3”: Clang, GCC, MSVC.
* Compilers that openly simulate GCC or MSVC via the ``CMAKE_<lang>_SIMULATE_ID`` variable are recognized as GCC compatible or MSVC compatible.
* Also recognizes the Intel compiler as GCC compatible on non-Windows platforms.

Usage::

    prac_detect_cxx_and_c_compilers()

Output variables for C++, set to either ``TRUE`` or ``FALSE``:

``PRAC_CXX_GCC_COMPAT``
    True if the compiler is GCC, or Clang in GCC-compatible mode, or another compiler reported as GCC compatible.
``PRAC_CXX_GCC``
    True if the compiler is the real GCC.
``PRAC_CXX_CLANG``
    True if the compiler is Clang, no matter in which mode.
``PRAC_CXX_MSVC_COMPAT``
    True if the compiler is MSVC, or Clang in MSVC-compatible command line mode (clang-cl), or
    another compiler reported as MSVC compatible.
``PRAC_CXX_MSVC``
    True if the compiler is the real MSVC.
``PRAC_CXX_COMPILER_DETECTED``
    True if any C++ compiler was detected, no matter which one.
``PRAC_CXX_COMPILER_PP_DEFINES``
    A list of preprocessor macro defines that can be used with ``target_compile_definitions()`` and accessed in the source code with ``#ifdef``. A macro is defined without a value for each of the above output variables set to ``TRUE``. For the other output variables no defines are created. The macro names are the same as the output variable names.

For C the same set of variables is defined with the prefix ``C_`` instead of ``CXX_``.

Calling this function multiple times is valid. For an already detected compiler the detection does not run again. Its variables remain unchanged.
]]
function(prac_detect_cxx_and_c_compilers)
    if (NOT DEFINED PROJECT_NAME)
        message(FATAL_ERROR "prac_detect_cxx_and_c_compilers() must be called after project().")
    endif()

    if (CMAKE_CXX_COMPILER_ID)
        pracprivate_detect_c_or_cxx_compiler(CXX)
    endif()

    if (CMAKE_C_COMPILER_ID)
        pracprivate_detect_c_or_cxx_compiler(C)
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Checks if a C++ compiler was detected with ``prac_detect_cxx_and_c_compilers()`` and aborts with a ``FATAL_ERROR`` if not. ::

    prac_verify_cxx_compiler_detected()
]]
macro(prac_verify_cxx_compiler_detected)
    if (NOT PRAC_CXX_COMPILER_DETECTED)
        message(FATAL_ERROR "Unclear C++ compiler. Call `prac_detect_cxx_and_c_compilers()` first.")
    endif()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Checks if a C compiler was detected with ``prac_detect_cxx_and_c_compilers()`` and aborts with a ``FATAL_ERROR`` if not. ::

    prac_verify_c_compiler_detected()
]]
macro(prac_verify_c_compiler_detected)
    if (NOT PRAC_C_COMPILER_DETECTED)
        message(FATAL_ERROR "Unclear C compiler. Call `prac_detect_cxx_and_c_compilers()` first.")
    endif()
endmacro()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Sets the required version of the C++ standard for a target. ::

    prac_target_cxx_standard(<target_name> {PUBLIC|PRIVATE|INTERFACE} <version> [EXTENSIONS])

Parameters:

``{PUBLIC|PRIVATE|INTERFACE}``
    the normal CMake scopes
``version``
    Accepted values are the same as for CMake’s ``CXX_STANDARD`` variable or the ``cxx_std_`` compile feature.
``EXTENSIONS``
    If given compiler extensions are allowed, otherwise they are prohibited.

The function handles both the classic triple of target properties (``CXX_STANDARD``, ``CXX_STANDARD_REQUIRED``, ``CXX_EXTENSIONS``) and the ``cxx_std_<version>`` compile feature as long as the CMake version supports it. The compile feature supports all scopes. Accordingly it is always configured for the given scope. The properties triple is private to the target. Following the usual CMake scope logic it is configured if ``PUBLIC`` or ``PRIVATE`` is given as a scope, but not touched for ``INTERFACE``.

Also the function takes some idiosyncrasies of MSVC++ into account:

* It always enables the alternative operator names mandated in [lex.digraph] in the standard, such as *not* instead of *!*. This is done either by force including the *iso646.h* header or by setting the */permissive-* option, depending on which is more appropriate for the given arguments.
* If the ``EXTENSIONS`` argument is missing and the compiler is at least MSVC++ 2017 it sets the */permissive-* switch to put the compiler into standard compliant mode.
]]
function(prac_target_cxx_standard target_name scope version)
    prac_verify_cxx_compiler_detected()

    if ("${scope}" STREQUAL "PUBLIC" OR "${scope}" STREQUAL "PRIVATE")
        set(scope_is_pub_or_priv TRUE)
        set(scope_is_intf FALSE)
    elseif ("${scope}" STREQUAL "INTERFACE")
        set(scope_is_pub_or_priv FALSE)
        set(scope_is_intf TRUE)
    endif()

    if (NOT (scope_is_pub_or_priv OR scope_is_intf))
        message(
            FATAL_ERROR
            "Unknown scope: ${scope}; expected one of: PUBLIC PRIVATE INTERFACE "
            "[target: ${target_name}]"
        )
    endif()

    if (ARGC GREATER 3)
        if (NOT "${ARGV3}" STREQUAL "EXTENSIONS")
            message(FATAL_ERROR "Unknown option: ${ARGV3} [target: ${target_name}]")
        endif()
        set(allow_extensions ON)
    else()
        set(allow_extensions OFF)
    endif()

    if (scope_is_pub_or_priv)
        set_target_properties(${target_name} PROPERTIES
            CXX_STANDARD ${version}
            CXX_STANDARD_REQUIRED ON
            CXX_EXTENSIONS ${allow_extensions}
        )
    endif()

    if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.8")
        target_compile_features(${target_name} ${scope} cxx_std_${version})
    endif()

    if (PRAC_CXX_MSVC)
        # MSVC special logic does two things. 1) It mimics CXX_EXTENSIONS as far as possible.
        # 2) It ensures support for the alternative operator names defined in [lex.digraph]
        # in the standard – e.g. *not* instead of *!*. (1) is only possible in a useful way with
        # the /permissive- switch available since Visual Studio 2017 (compiler version 19.10).
        # (2) can be achieved either through /permissive- or through force-including the iso646.h
        # header.
        if (NOT allow_extensions AND ${MSVC_VERSION} GREATER_EQUAL 1910)
            target_compile_options(${target_name} ${scope} /permissive-)
        else()
            target_compile_options(${target_name} ${scope} /FIiso646.h)
        endif()
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Sets the required version of the C standard for a target. ::

    prac_target_c_standard(<target_name> {PUBLIC|PRIVATE|INTERFACE} <version> [EXTENSIONS])

Parameters:

``{PUBLIC|PRIVATE|INTERFACE}``
    the normal CMake scopes
``version``
    Accepted values are the same as for CMake’s ``C_STANDARD`` variable or the ``c_std_`` compile feature.
``EXTENSIONS``
    If given compiler extensions are allowed, otherwise they are prohibited.

The function handles both the classic triple of target properties (``C_STANDARD``, ``C_STANDARD_REQUIRED``, ``C_EXTENSIONS``) and the ``c_std_<version>`` compile feature as long as the CMake version supports it. The compile feature supports all scopes. Accordingly it is always configured for the given scope. The properties triple is private to the target. Following the usual CMake scope logic it is configured if ``PUBLIC`` or ``PRIVATE`` is given as a scope, but not touched for ``INTERFACE``.
]]
function(prac_target_c_standard target_name scope version)
    prac_verify_c_compiler_detected()

    if ("${scope}" STREQUAL "PUBLIC" OR "${scope}" STREQUAL "PRIVATE")
        set(scope_is_pub_or_priv TRUE)
        set(scope_is_intf FALSE)
    elseif ("${scope}" STREQUAL "INTERFACE")
        set(scope_is_pub_or_priv FALSE)
        set(scope_is_intf TRUE)
    endif()

    if (NOT (scope_is_pub_or_priv OR scope_is_intf))
        message(
            FATAL_ERROR
            "Unknown scope: ${scope}; expected one of: PUBLIC PRIVATE INTERFACE "
            "[target: ${target_name}]"
        )
    endif()

    if (ARGC GREATER 3)
        if (NOT "${ARGV3}" STREQUAL "EXTENSIONS")
            message(FATAL_ERROR "Unknown option: ${ARGV2} [target: ${target_name}]")
        endif()
        set(allow_extensions ON)
    else()
        set(allow_extensions OFF)
    endif()

    if (scope_is_pub_or_priv)
        set_target_properties(${target_name} PROPERTIES
            C_STANDARD ${version}
            C_STANDARD_REQUIRED ON
            C_EXTENSIONS ${allow_extensions}
        )
    endif()

    if (CMAKE_VERSION VERSION_GREATER_EQUAL "3.8")
        target_compile_features(${target_name} ${scope} c_std_${version})
    endif()
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Safely appends link flags to the existing link flags of the given target. ::

    prac_target_link_flags(<target_name> <flag1> ...)

The ``LINK_FLAGS`` target property is a space separated string. That does not fit CMake’s list handling semantics and makes appending by hand awkward and error prone. Also, we have ``target_compile_options()`` and friends. Having a similar function for the linker just makes sense.

Note: Since CMake 3.13 ``target_link_options()`` exists and should be preferred.
]]
function(prac_target_link_flags target_name)
    list(JOIN ARGN " " flags)
    set_property(TARGET ${target_name} APPEND_STRING PROPERTY LINK_FLAGS " ${flags}")
endfunction()


# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#[[.rst:
Sets the symbol visibility for ``<target_name>`` which must be a C++ or C target. ::

    prac_target_symbol_visibility(<target_name> <visibility> [HIDDEN_INLINES])

Parameters:

``<visibility>``
    Symbol visibility mode. Pass the same value you would set the ``<LANG>_VISIBILITY_PRESET`` property to.
``HIDDEN_INLINES``
    If present configures the compiler to hide symbols of inline functions.

If you also want to create a header file with the usual set of export/API macros, consider using ``prac_configure_library_api()`` from the ``LibraryApi`` module instead.
]]
function(prac_target_symbol_visibility target_name visi)
    if (${ARGC} GREATER 2)
        if (NOT "${ARGV2}" STREQUAL "HIDDEN_INLINES")
            message(FATAL_ERROR "Unknown argument: ${ARGV2}")
        endif()
        set(inline_visi TRUE)
    else()
        set(inline_visi FALSE)
    endif()

    set_target_properties(${target_name} PROPERTIES
        CXX_VISIBILITY_PRESET ${visi}
        C_VISIBILITY_PRESET ${visi}
        VISIBILITY_INLINES_HIDDEN ${inline_visi}
    )
endfunction()


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# implementation details

function(pracprivate_decolon_namesapce ns)
    if ("${ns}" STREQUAL "::")
        message(FATAL_ERROR "Namespace consists of only a doulbe colon `::`.")
    endif()

    string(REPLACE "::" "" ns "${ns}")
    set(pracprivate_nsname "${ns}" PARENT_SCOPE)
endfunction()


function(pracprivate_parse_ns_args target_name)
    cmake_parse_arguments(PARSE_ARGV 1 "" "" "NAMESPACE" "")

    if ("${_NAMESPACE}" STREQUAL "")
        if ("${${PROJECT_NAME}_NAMESPACE}" STREQUAL "")
            set(_NAMESPACE "${PROJECT_NAME}")
        else()
            set(_NAMESPACE "${${PROJECT_NAME}_NAMESPACE}")
        endif()
    endif()

    pracprivate_decolon_namesapce("${_NAMESPACE}")

    set(pracprivate_undername "${pracprivate_nsname}_${target_name}" PARENT_SCOPE)
    set(pracprivate_colonname "${pracprivate_nsname}::${target_name}" PARENT_SCOPE)
    set(pracprivate_passthrough_args ${_UNPARSED_ARGUMENTS} PARENT_SCOPE)
endfunction()


macro(pracprivate_detect_c_or_cxx_compiler lang)
    if (NOT PRAC_${lang}_COMPILER_DETECTED)
        set(PRAC_${lang}_COMPILER_DETECTED TRUE PARENT_SCOPE)
        set(PRAC_${lang}_GCC_COMPAT FALSE PARENT_SCOPE)
        set(PRAC_${lang}_GCC FALSE PARENT_SCOPE)
        set(PRAC_${lang}_CLANG FALSE PARENT_SCOPE)
        set(PRAC_${lang}_MSVC_COMPAT FALSE PARENT_SCOPE)
        set(PRAC_${lang}_MSVC FALSE PARENT_SCOPE)
        set(PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_COMPILER_DETECTED")

        if (CMAKE_${lang}_COMPILER_ID STREQUAL "GNU")
            set(PRAC_${lang}_GCC TRUE PARENT_SCOPE)
            set(PRAC_${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES
                "PRAC_${lang}_GCC"
                "PRAC_${lang}_GCC_COMPAT"
            )

        elseif (CMAKE_${lang}_COMPILER_ID MATCHES "Clang$") # need fuzzy match to catch Apple Clang
            set(PRAC_${lang}_CLANG TRUE PARENT_SCOPE)
            list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_CLANG")

            if (CMAKE_${lang}_SIMULATE_ID STREQUAL "MSVC")  # catch clang-cl
                set(PRAC_${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
                list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_MSVC_COMPAT")
            else()
                set(PRAC_${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
                list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_GCC_COMPAT")
            endif()

        elseif (CMAKE_${lang}_COMPILER_ID STREQUAL "MSVC")
            set(PRAC_${lang}_MSVC TRUE PARENT_SCOPE)
            set(PRAC_${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES
                "PRAC_${lang}_MSVC"
                "PRAC_${lang}_MSVC_COMPAT"
            )

        elseif (NOT WIN32 AND CMAKE_${lang}_COMPILER_ID STREQUAL "Intel")
            set(PRAC_${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_GCC_COMPAT")
        endif()


        if (CMAKE_${lang}_SIMULATE_ID STREQUAL "GNU")
            set(PRAC_${lang}_GCC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_GCC_COMPAT")
        elseif (CMAKE_${lang}_SIMULATE_ID STREQUAL "MSVC")
            set(PRAC_${lang}_MSVC_COMPAT TRUE PARENT_SCOPE)
            list(APPEND PRAC_${lang}_COMPILER_PP_DEFINES "PRAC_${lang}_MSVC_COMPAT")
        endif()

        list(REMOVE_DUPLICATES PRAC_${lang}_COMPILER_PP_DEFINES)
        set(PRAC_${lang}_COMPILER_PP_DEFINES ${PRAC_${lang}_COMPILER_PP_DEFINES} PARENT_SCOPE)
    endif()
endmacro()
